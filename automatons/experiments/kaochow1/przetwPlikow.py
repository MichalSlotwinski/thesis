#!/usr/bin/env python3

import sys
import os
import subprocess as sp

def changeStructureOfDirs(dirName):
    os.chdir(dirName)
    dirList  = os.walk(".")
    for root, dirs, files in dirList:
        for fileName in files:
            if fileName[-3:] == "nta":
                try:
                    sp.check_output("mkdir " + fileName[:len(fileName)-4], shell = True)
                    sp.check_output("mv " + fileName[:len(fileName)-4] + ".nta " + fileName[:len(fileName)-4], shell = True)
                    sp.check_output("mv " + fileName[:len(fileName)-4] + ".efo " + fileName[:len(fileName)-4], shell = True)
                except sp.CalledProcessError as err:
                    print("Problem with moving files")
                    exit(2) 
                    
def main():
    changeStructureOfDirs(".")
    
main()
