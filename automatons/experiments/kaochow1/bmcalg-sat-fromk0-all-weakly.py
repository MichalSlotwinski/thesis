#!/usr/bin/env python3

import sys
import os
import subprocess


dirList  = os.walk('.')


def createBMCcommand(k, BIN, NET, FORM, dirName):
    BLOG = NET + "-bmc-k" + str(k) + ".log"
    cmd = "/usr/bin/time"  
    cmd = cmd + " -o " + dirName +"/" +BLOG + " -f \"%M %U %S KB\" "
    cmd = cmd + BIN +"/satbmc4dta --weakly " + dirName + "/" + NET + " " 
    cmd = cmd + dirName + "/" + FORM
    cmd = cmd + " " + str(k) + " >| " + dirName + "/" + NET + "-bmc-k" + str(k) + ".out"
    return cmd

def executeBMCCommand(cmdBMC):
    try:
        subprocess.check_output(cmdBMC, shell = True)
    except subprocess.CalledProcessError as err:
        print("SAT4DTA failure")
        exit(2)
        
def createSATcommand(k, NET, dirName):
    SLOG = dirName + "/" + NET + "-sat-k" + str(k) + ".log"
    cmd = "/usr/bin/time -o " + SLOG + " -f \"%M %U %S KB\" minisat2 " 
    cmd = cmd + dirName + "/" +NET + "-k" + str(k) + ".cnf > " 
    cmd = cmd + dirName + "/" + NET + "-k" + str(k) + ".out"
    print(cmd)
    return cmd


def executeSATCommand(cmdSAT):
    try:
        result = subprocess.check_output(cmdSAT, shell = True)
    except subprocess.CalledProcessError as err:
        return err.returncode

def headCommand(dirName, NET, k):
    cmd = "tail -1 " + dirName + "/" + NET + "-k" + str(k) + ".out"
    return cmd 
    
                      
def deleteSATFile(dirName, NET, k):
    try:
        subprocess.check_output("rm -f " + dirName + "/" + NET + "-k" + str(k) + ".cnf", shell = True)
    except subprocess.CalledProcessError as err:
        print("rm cnf file failure")
        exit(5)

def headResult(headCmd):
    try:
        result = subprocess.check_output(headCmd, universal_newlines=True, shell = True)
        return result
    except subprocess.CalledProcessError as err:
        print("head failure")
        exit(4)
    
def main():
    k = 0
    BIN = "../../../bin"
    
    for root, dirs, files in dirList:
        for dirName in dirs:
            NET = dirName 
            FORM = dirName    
            print("benchmark: " + dirName)
            while True:                
                cmdBMC = createBMCcommand(k, BIN, NET, FORM, dirName)
                executeBMCCommand(cmdBMC)
                            
                print("Checking satisfiability ... ")
                               
                cmdSAT = createSATcommand(k, NET, dirName)
                result = executeSATCommand(cmdSAT)
                deleteSATFile(dirName, NET, k)   
    
                if result == 10:
                    print ("SATISFIABLE\n")
                    print ()
                    print("The property is REACHABLE")
                    break
                elif  result == 20:
                    print("UNSATISFIABLE\n")
                else:
                    print("UNKNOWN\n")
                k = k + 1
            k = 0
main()
