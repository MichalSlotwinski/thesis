package storage.enums;

public enum Column
{
    FILENAME("Filename", 0),
    BUILD_TYPE("Build Type", 1),
    GRAPH_NODE_COUNT("Graph Node Count", 2),
    GRAPH_TRANSITION_COUNT("Graph Transition Count", 3),
    GRAPH_UNIQUE_TRANSITION_COUNT("Graph Unique Transition Count", 4),
    NETWORK_NODE_COUNT("Network Node Count", 5),
    NETWORK_TRANSITION_COUNT("Network Transition Count", 6),
    NETWORK_UNIQUE_TRANSITION_COUNT("Network Unique Transition Count", 7),
    USED_NODE_COUNT("Used Node Count", 8),
    UNUSED_NODE_COUNT("Unused Node Count", 9),
    USED_TRANSITION_COUNT("Used Transition Count", 10),
    UNUSED_TRANSITION_COUNT("Unused Transition Count", 11),
    TRACK_MODE_1("Track Mode", 12),
    STOP_VALUE_1("Stop Value", 13),
    ACTUAL_VALUE("Actual Stop Value", 14),
    TRACK_MODE_2("Track Mode", 15),
    STOP_VALUE_2("Stop Value", 16),
    IS_REACHABLE("Is Reachable", 17),
    ALL_PATHS("All Paths", 18);

    private final String value;
    private final Integer id;

    Column(final String value, final Integer id)
    {
        this.value = value;
        this.id = id;
    }

    public String getValue() { return this.value; }

    public Integer getId() { return id; }
}
