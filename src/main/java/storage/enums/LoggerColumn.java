package storage.enums;

public enum LoggerColumn
{
    TIME("Time"),
    NODES("States"),
    TRANSITIONS("Transitions");

    private final String value;

    LoggerColumn(final String value)
    {
        this.value = value;
    }

    public String getValue() { return this.value; }
}
