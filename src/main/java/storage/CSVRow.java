package storage;


import UI.views.enums.BuildType;
import automaton.algorithm.ProductBuilder;
import automaton.algorithm.Statistic;
import automaton.util.TrackMode;
import automaton.util.Triple;
import storage.enums.Column;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CSVRow
{
    String                                              filename;
    BuildType                                           buildType;
    Integer                                             graphNodeCount;
    Integer                                             graphTransitionCount;
    Integer                                             graphUniqueTransitionCount;
    Integer                                             networkNodeCount;
    Integer                                             networkTransitionCount;
    Integer                                             networkUniqueTransitionCount;
    Integer                                             usedNodeCount;
    Integer                                             unusedNodeCount;
    Integer                                             usedTransitionCount;
    Integer                                             unusedTransitionCount;

    TrackMode                                           trackMode1;
    Integer                                             stopValue1;
    Integer                                             actualValue;
    TrackMode                                           trackMode2;
    Integer                                             stopValue2;

    List<Triple<Integer, Integer, Boolean>>             isReachable;
    List<Triple<Integer, Integer, List<List<Integer>>>> allPaths;


    public CSVRow(String line)
    {
        parse(line);
    }

    public CSVRow(File file, Statistic statistic, ProductBuilder builder)
    {
        filename = file.getName();
        this.buildType = builder.getType();
        graphNodeCount = statistic.countGraphNodes();
        graphTransitionCount = statistic.countGraphTransitions();
        graphUniqueTransitionCount = statistic.countUniqueGraphTransitions();
        networkNodeCount = statistic.countNetworkNodes();
        networkTransitionCount = statistic.countNetworkTransitions();
        networkUniqueTransitionCount = statistic.countUniqueNetworkTransitions();
        usedNodeCount = statistic.countUsedNodes();
        unusedNodeCount = statistic.countUnusedNodes();
        usedTransitionCount = statistic.countUsedTransitions();
        unusedTransitionCount = statistic.countUnusedTransitions();
        this.trackMode1 = builder.getTrackMode1();
        this.stopValue1 = builder.getStopValue1();
        this.actualValue = builder.getActualStopValue();
        this.trackMode2 = builder.getTrackMode2();
        this.stopValue2 = builder.getStopValue2();

        isReachable = new ArrayList<>();
        allPaths = new ArrayList<>();
    }

    public CSVRow(Statistic statistic, CSVRow row, List<Column> columns, Integer src, Integer dest)
    {
        filename = row.filename;
        this.buildType = row.buildType;
        graphNodeCount = columns.contains(Column.GRAPH_NODE_COUNT) ? statistic.countGraphNodes() : row.graphNodeCount;
        graphTransitionCount = columns.contains(Column.GRAPH_TRANSITION_COUNT) ? statistic.countGraphTransitions() : row.graphTransitionCount;
        graphUniqueTransitionCount = columns.contains(Column.GRAPH_UNIQUE_TRANSITION_COUNT) ? statistic.countUniqueGraphTransitions() : row.graphUniqueTransitionCount;
        networkNodeCount = columns.contains(Column.NETWORK_NODE_COUNT) ? statistic.countNetworkNodes() : row.networkNodeCount;
        networkTransitionCount = columns.contains(Column.NETWORK_TRANSITION_COUNT) ? statistic.countNetworkTransitions() : row.networkTransitionCount;
        networkUniqueTransitionCount = columns.contains(Column.NETWORK_UNIQUE_TRANSITION_COUNT) ? statistic.countUniqueNetworkTransitions() : row.networkUniqueTransitionCount;
        usedNodeCount = columns.contains(Column.USED_NODE_COUNT) ? statistic.countUsedNodes() : row.usedNodeCount;
        unusedNodeCount = columns.contains(Column.UNUSED_NODE_COUNT) ? statistic.countUnusedNodes() : row.unusedNodeCount;
        usedTransitionCount = columns.contains(Column.USED_TRANSITION_COUNT) ? statistic.countUsedTransitions() : row.usedTransitionCount;
        unusedTransitionCount = columns.contains(Column.UNUSED_TRANSITION_COUNT) ? statistic.countUnusedTransitions() : row.unusedTransitionCount;
        this.trackMode1 = row.trackMode1;
        this.stopValue1 = row.stopValue1;
        this.actualValue = row.actualValue;
        this.trackMode2 = row.trackMode2;
        this.stopValue2 = row.stopValue2;

        this.isReachable = row.isReachable;
        if(columns.contains(Column.IS_REACHABLE))
            addReachability(statistic, src, dest);

        this.allPaths = row.allPaths;
        if(columns.contains(Column.ALL_PATHS))
            addPaths(statistic, src, dest);
    }

    public void addReachability(Statistic statistic, Integer src, Integer dest)
    {
        isReachable.add(new Triple<>(src, dest, statistic.isReachable(src, dest)));
    }

    public void addPaths(Statistic statistic, Integer src, Integer dest)
    {
        allPaths.add(new Triple<>(src, dest, statistic.getAllPaths(src, dest)));
    }

    public void parse(String line)
    {
        String[] tokens = line.split("\",\"");
        tokens = Arrays.stream(tokens).map(s -> s.replaceAll("\"", "")).toArray(String[]::new);
        if(tokens.length == Column.values().length)
        {
            filename = tokens[Column.FILENAME.getId()];
            buildType = BuildType.valueOf(tokens[Column.BUILD_TYPE.getId()]);
            graphNodeCount = Integer.parseInt(tokens[Column.GRAPH_NODE_COUNT.getId()]);
            graphTransitionCount = Integer.parseInt(tokens[Column.GRAPH_TRANSITION_COUNT.getId()]);
            graphUniqueTransitionCount = Integer.parseInt(tokens[Column.GRAPH_UNIQUE_TRANSITION_COUNT.getId()]);
            networkNodeCount = Integer.parseInt(tokens[Column.NETWORK_NODE_COUNT.getId()]);
            networkTransitionCount = Integer.parseInt(tokens[Column.NETWORK_TRANSITION_COUNT.getId()]);
            networkUniqueTransitionCount = Integer.parseInt(tokens[Column.NETWORK_UNIQUE_TRANSITION_COUNT.getId()]);
            usedNodeCount = Integer.parseInt(tokens[Column.USED_NODE_COUNT.getId()]);
            unusedNodeCount = Integer.parseInt(tokens[Column.UNUSED_NODE_COUNT.getId()]);
            usedTransitionCount = Integer.parseInt(tokens[Column.USED_TRANSITION_COUNT.getId()]);
            unusedTransitionCount = Integer.parseInt(tokens[Column.UNUSED_TRANSITION_COUNT.getId()]);

            trackMode1 = TrackMode.valueOf(tokens[Column.TRACK_MODE_1.getId()]);
            String stopValue1Str = tokens[Column.STOP_VALUE_1.getId()];
            stopValue1 = stopValue1Str.isEmpty() ? null : Integer.parseInt(stopValue1Str);
            String actualValueStr = tokens[Column.ACTUAL_VALUE.getId()];
            actualValue = actualValueStr.isEmpty() ? null : Integer.parseInt(actualValueStr);

            trackMode2 = TrackMode.valueOf(tokens[Column.TRACK_MODE_2.getId()]);
            String stopValue2Str = tokens[Column.STOP_VALUE_2.getId()];
            stopValue2 = stopValue2Str.isEmpty() ? null : Integer.parseInt(stopValue2Str);

            isReachable = parseIsReachable(tokens[Column.IS_REACHABLE.getId()]);
            allPaths = parsePaths(tokens[Column.ALL_PATHS.getId()]);
        }
    }

    private List<Integer> parseList(String list)
    {
        list = list.replaceAll("[\\[\\]]", "");

        if(list.isEmpty())
            return new ArrayList<>();

        String[] elements = list.split(", ");
        return Arrays.stream(elements)
                     .map(Integer::parseInt)
                     .collect(Collectors.toList());
    }

    private List<Triple<Integer, Integer, Boolean>> parseIsReachable(String reachability)
    {
        if(reachability.isEmpty())
            return new ArrayList<>();

        List<Triple<Integer, Integer, Boolean>> list = new ArrayList<>();
        String[] tokens = reachability.split("\\|");
        for(String token : tokens)
        {
            String[] elem = token.split("(->|:)");
            list.add(new Triple<>(Integer.parseInt(elem[0]), Integer.parseInt(elem[1]), Boolean.parseBoolean(elem[2])));
        }

        return list;
    }

    private List<Triple<Integer, Integer, List<List<Integer>>>> parsePaths(String paths)
    {
        if(paths.isEmpty())
            return new ArrayList<>();

        List<Triple<Integer, Integer, List<List<Integer>>>> allPaths = new ArrayList<>();
        String[] tokens = paths.split("\\|");
        for(String token : tokens)
        {
            String[] elem = token.split("(->|:)");
            String[] lists = elem[2].substring(1, elem[2].length() - 1).split("],\\[");
            List<List<Integer>> pathList = new ArrayList<>();
            for(String list : lists)
            {
                pathList.add(parseList(list.replaceAll("\\[]", "").trim()));
            }
            allPaths.add(new Triple<>(Integer.parseInt(elem[0]), Integer.parseInt(elem[1]), pathList));
        }

        return allPaths;
    }

    private String quote(Object object)
    {
        return CSVMapper.QUOT + object.toString() + CSVMapper.QUOT;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(quote(filename)).append(CSVMapper.SEP)
          .append(quote(buildType)).append(CSVMapper.SEP)
          .append(quote(graphNodeCount)).append(CSVMapper.SEP)
          .append(quote(graphTransitionCount)).append(CSVMapper.SEP)
          .append(quote(graphUniqueTransitionCount)).append(CSVMapper.SEP)
          .append(quote(networkNodeCount)).append(CSVMapper.SEP)
          .append(quote(networkTransitionCount)).append(CSVMapper.SEP)
          .append(quote(networkUniqueTransitionCount)).append(CSVMapper.SEP)
          .append(quote(usedNodeCount)).append(CSVMapper.SEP)
          .append(quote(unusedNodeCount)).append(CSVMapper.SEP)
          .append(quote(usedTransitionCount)).append(CSVMapper.SEP)
          .append(quote(unusedTransitionCount)).append(CSVMapper.SEP)
          .append(quote(trackMode1.toString())).append(CSVMapper.SEP)
          .append(quote(stopValue1 == null ? "" : stopValue1)).append(CSVMapper.SEP)
          .append(quote(actualValue == null ? "" : actualValue)).append(CSVMapper.SEP)
          .append(quote(trackMode2.toString())).append(CSVMapper.SEP)
          .append(quote(stopValue2 == null ? "" : stopValue2)).append(CSVMapper.SEP)

          .append(quote(isReachableToString()))   // 1->2:true|1->3:false
          .append(CSVMapper.SEP)
          .append(quote(allPathsToString()));     // 1->2:{[1,2,3,4,5],[1,2,8,4,5]}|1->3:{[]}

        return sb.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        CSVRow row = (CSVRow) o;

        return Objects.equals(filename, row.filename) &&
               Objects.equals(buildType, row.buildType) &&
               Objects.equals(graphNodeCount, row.graphNodeCount) && 
               Objects.equals(graphTransitionCount, row.graphTransitionCount) && 
               Objects.equals(graphUniqueTransitionCount, row.graphUniqueTransitionCount) && 
               Objects.equals(networkNodeCount, row.networkNodeCount) && 
               Objects.equals(networkTransitionCount, row.networkTransitionCount) && 
               Objects.equals(networkUniqueTransitionCount, row.networkUniqueTransitionCount) && 
               Objects.equals(usedNodeCount, row.usedNodeCount) && 
               Objects.equals(unusedNodeCount, row.unusedNodeCount) && 
               Objects.equals(usedTransitionCount, row.usedTransitionCount) && 
               Objects.equals(unusedTransitionCount, row.unusedTransitionCount) &&
               Objects.equals(trackMode1, row.trackMode1) &&
               Objects.equals(stopValue1, row.stopValue1) &&
               trackMode1 == TrackMode.TIME ? Math.abs(actualValue - row.actualValue) < 1000
                                           : Objects.equals(actualValue, row.actualValue) &&
               Objects.equals(trackMode2, row.trackMode2) &&
               Objects.equals(stopValue2, row.stopValue2) &&
               Objects.equals(isReachable, row.isReachable) &&
               Objects.equals(allPaths, row.allPaths);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(filename,
                            buildType,
                            graphNodeCount,
                            graphTransitionCount,
                            graphUniqueTransitionCount,
                            networkNodeCount,
                            networkTransitionCount,
                            networkUniqueTransitionCount,
                            usedNodeCount,
                            unusedNodeCount,
                            usedTransitionCount,
                            unusedTransitionCount,
                            trackMode1,
                            stopValue1,
                            actualValue,
                            trackMode2,
                            stopValue2,
                            isReachable,
                            allPaths);
    }

    public String isReachableToString()
    {
        StringBuilder sb = new StringBuilder();
        for(Triple<Integer, Integer, Boolean> elem : isReachable)
            sb.append(elem.first())
              .append("->")
              .append(elem.second())
              .append(":")
              .append(elem.third())
              .append(CSVMapper.INNER_DELIM);

        if(!isReachable.isEmpty())
            sb.deleteCharAt(sb.lastIndexOf(Character.toString(CSVMapper.INNER_DELIM)));

        return sb.toString();
    }

    public String allPathsToString()
    {
        StringBuilder sb = new StringBuilder();
        for(Triple<Integer, Integer, List<List<Integer>>> elem : allPaths)
        {
            sb.append(elem.first())
              .append("->")
              .append(elem.second())
              .append(":{");
            for(List<Integer> path : elem.third())
            {
                sb.append(path.toString())
                  .append(CSVMapper.SEP);
            }
            sb.deleteCharAt(sb.lastIndexOf(Character.toString(CSVMapper.SEP)));
            sb.append("}")
              .append(CSVMapper.INNER_DELIM);
        }

        if(!allPaths.isEmpty())
            sb.deleteCharAt(sb.lastIndexOf(Character.toString(CSVMapper.INNER_DELIM)));

        return sb.toString();
    }

    public Object get(Column column) throws NoSuchFieldException
    {
        switch(column)
        {
            case FILENAME: return filename;
            case BUILD_TYPE: return buildType;
            case GRAPH_NODE_COUNT: return graphNodeCount;
            case GRAPH_TRANSITION_COUNT: return graphTransitionCount;
            case GRAPH_UNIQUE_TRANSITION_COUNT: return graphUniqueTransitionCount;
            case NETWORK_NODE_COUNT: return networkNodeCount;
            case NETWORK_TRANSITION_COUNT: return networkTransitionCount;
            case NETWORK_UNIQUE_TRANSITION_COUNT: return networkUniqueTransitionCount;
            case USED_NODE_COUNT: return usedNodeCount;
            case UNUSED_NODE_COUNT: return unusedNodeCount;
            case USED_TRANSITION_COUNT: return usedTransitionCount;
            case UNUSED_TRANSITION_COUNT: return unusedTransitionCount;
            case TRACK_MODE_1: return trackMode1;
            case STOP_VALUE_1: return stopValue1;
            case TRACK_MODE_2: return trackMode2;
            case STOP_VALUE_2: return stopValue2;
            case ACTUAL_VALUE: return actualValue;
            case IS_REACHABLE: return isReachable;
            case ALL_PATHS: return allPaths;
            default:
                throw new NoSuchFieldException("This class does not have such field.");
        }
    }
}
