package storage;

import storage.enums.Column;

import java.io.*;
import java.nio.file.Paths;

public class CSVMapper
{
    /** 'LF' */
    public static final char NL = '\n';
    /** 44 */
    public static final char SEP = ',';
    /** 34 */
    public static final char QUOT = '"';
    /** 34 */
    public static final char ESC = '"';
    /** 124 */
    public static final char INNER_DELIM = '|';


    private final File csv;
    private final File tmp;

    public CSVMapper(File file)
    {
        this.csv = file;
        this.tmp = Paths.get(file.getParent(), "temp.tmp").toFile();
    }

    public void header() throws IOException
    {
        StringBuilder sb = new StringBuilder();
        for(Column column : Column.values())
            sb.append(quote(column.getValue()))
              .append(SEP);
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append(NL);

        BufferedWriter writer = new BufferedWriter(new FileWriter(this.csv));
        writer.write(sb.toString());
        writer.close();
    }

    public File getCsv() { return this.csv; }

    public synchronized void add(CSVRow row) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        sb.append(row).append(NL);

        BufferedWriter writer = new BufferedWriter(new FileWriter(this.csv, true));
        writer.write(sb.toString());
        writer.close();
    }

    public CSVRow get(String filename, boolean loose) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(this.csv));
        String line;
        while((line = reader.readLine()) != null)
        {
            if(loose)
            {
                if(line.startsWith("\"" + filename))
                {
                    reader.close();
                    return new CSVRow(line);
                }
            }
            else
            {
                if(line.startsWith(quote(filename)))
                {
                    reader.close();
                    return new CSVRow(line);
                }
            }
        }
        reader.close();
        return null;
    }

    public void replace(String filename, CSVRow replacement) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(this.csv));
        BufferedWriter writer = new BufferedWriter(new FileWriter(this.tmp));
        String line;
        while((line = reader.readLine()) != null)
        {
            if(!line.startsWith(quote(filename)))
                writer.write(line);
            else
                writer.write(replacement.toString());
            writer.write(NL);
        }
        writer.close();
        reader.close();
        this.csv.delete();
        this.tmp.renameTo(this.csv);
    }

    public void remove(String filename) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(this.csv));
        BufferedWriter writer = new BufferedWriter(new FileWriter(this.tmp));
        String line;
        while((line = reader.readLine()) != null)
        {
            if(!line.startsWith(quote(filename)))
            {
                writer.write(line);
                writer.write(NL);
            }
        }
        writer.close();
        reader.close();
        this.csv.delete();
        this.tmp.renameTo(this.csv);
    }

    private String quote(Object object)
    {
        return CSVMapper.QUOT + object.toString() + CSVMapper.QUOT;
    }
}
