package storage;

import storage.enums.Column;
import storage.enums.LoggerColumn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CSVExecLogger
{
    private File csv;

    public CSVExecLogger(File csv)
    {
        this.csv = csv;
    }

    public void header() throws IOException
    {
        StringBuilder sb = new StringBuilder();
        for(LoggerColumn column : LoggerColumn.values())
            sb.append(quote(column.getValue()))
              .append(CSVMapper.SEP);
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append(CSVMapper.NL);

        BufferedWriter writer = new BufferedWriter(new FileWriter(this.csv));
        writer.write(sb.toString());
        writer.close();
    }

    public void add(Long time, Integer nodes, Integer transitions) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        sb.append(time).append(CSVMapper.SEP)
          .append(nodes).append(CSVMapper.SEP)
          .append(transitions).append(CSVMapper.NL);

        BufferedWriter writer = new BufferedWriter(new FileWriter(this.csv, true));
        writer.write(sb.toString());
        writer.close();
    }

    private String quote(Object object)
    {
        return CSVMapper.QUOT + object.toString() + CSVMapper.QUOT;
    }
}
