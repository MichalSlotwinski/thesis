package UI.views;

import UI.Controller;
import UI.UIScene;
import UI.views.enums.Document;
import UI.views.exceptions.IncompatibleNamesException;
import automaton.model.Graph;
import automaton.model.Network;
import file.utils.FileUtils;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.xml.sax.SAXException;
import storage.CSVRow;
import storage.enums.Column;
import task.ComputeTask;
import task.ExploreTask;
import task.util.DrawUtils;
import xml.GraphType;
import xml.XmlParser;
import xml.exceptions.NoSuchGraphType;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ExploreView implements UIScene
{
    private final Stage primaryStage;
    private Parent startView;
    private final Label title, statusNetwork, statusGraph;
    private final TextField inputPath;
    private final Button explore, back, exit, input, displayData, load, select, unselect;
    private final CheckBox writeToCsv;
    private final Button draw;
    private final ComboBox<Document> loadDocument;
    private final ListView<Column> columnsToSelect, selectedColumns;
    private final GridPane gridPane;
    private final BorderPane borderPane;
    private Network network;
    private Graph graph;

    private String graphFilename = null, networkFilename = null;
    private boolean inControl = true, loadControl = true;
    private ExploreTask task = null;

    public ExploreView(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
        this.gridPane = new GridPane();
        this.borderPane = new BorderPane();
        this.title = new Label();
        this.statusNetwork = new Label();
        this.statusGraph = new Label();
        this.inputPath = new TextField();
        this.writeToCsv = new CheckBox();
        this.draw = new Button();
        this.loadDocument = new ComboBox<>();
        this.columnsToSelect = new ListView<>();
        this.selectedColumns = new ListView<>();
        this.explore = new Button();
        this.back = new Button();
        this.exit = new Button();
        this.input = new Button();
        this.displayData = new Button();
        this.load = new Button();
        this.select = new Button();
        this.unselect = new Button();

        this.network = null;
        this.graph = null;
    }

    @Override
    public Parent makeView()
    {
        styleTitleLabel();
        styleStatusNetworkLabel();
        styleStatusGraphLabel();
        styleLoadDocumentComboBox();
        styleColumnsToSelectListView();
        styleSelectedColumnsListView();
        styleLoadButton();
        styleSelectButton();
        styleUnselectButton();
        styleInputPathTextField();
        styleInputButton();
        styleWriteToCsvCheckBox();
        styleDrawCheckBox();
        styleDisplayData();
        styleExploreButton();
        styleBackButton();
        styleExitButton();
        constraintGridPane(this.gridPane);
        addElementsToGridPane();
        constraintBorderPane();
        disableControls(true);
        return this.borderPane;
    }

    public void setStartView(Parent startView) { this.startView = startView; }

    public void setGraph(Graph graph) { this.graph = graph; }

    public void setNetwork(Network network) { this.network = network; }

    private void disableControls(boolean disable)
    {
        this.writeToCsv.setDisable(disable);
        this.draw.setDisable(disable);
        this.displayData.setDisable(disable);
    }

    private FileChooser getFileChooserGraphML()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All files", "*.graphml"),
                new FileChooser.ExtensionFilter("GraphML", "*.graphml")
        );
        return fileChooser;
    }

    private FileChooser getFileChooserCSV()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All files", "*.csv"),
                new FileChooser.ExtensionFilter("GraphML", "*.csv")
        );
        return fileChooser;
    }

    private void loadGraphs(File file) throws NoSuchGraphType, IOException, ParserConfigurationException, SAXException, IncompatibleNamesException
    {
        if(this.loadDocument.getValue() == Document.DETERMINE)
        {
            GraphType type = XmlParser.determineType(file);
            switch(type)
            {
                case NETWORK:
                    this.network = XmlParser.parseNetwork(file);
                    this.networkFilename = file.getName();
                    if(!this.checkNames())
                    {
                        this.network = null;
                        throw new IncompatibleNamesException("Incompatible names");
                    }
                    else
                    {
                        this.statusNetwork.setText("Network: " + file.getName() + ".");
                    }
                    break;
                case GRAPH:
                    this.graph = XmlParser.parseGraph(file);
                    this.graphFilename = file.getName();
                    if(!this.checkNames())
                    {
                        this.graph = null;
                        throw new IncompatibleNamesException("Incompatible names");
                    }
                    else
                    {
                        this.statusGraph.setText("Graph: " + file.getName() + ".");
                    }
                    break;
                default:
                    throw new NoSuchGraphType("Unknown graph type.");
            }
        }
        else if(this.loadDocument.getValue() == Document.NETWORK)
        {
            this.network = XmlParser.parseNetwork(file);
            this.networkFilename = file.getName();
            if(!this.checkNames())
            {
                this.network = null;
                throw new IncompatibleNamesException("Incompatible names");
            }
            else
            {
                this.statusNetwork.setText("Network: " + file.getName() + ".");
            }
            this.statusNetwork.setText("Network: " + file.getName() + ".");
        }
        else if(this.loadDocument.getValue() == Document.GRAPH)
        {
            this.graph = XmlParser.parseGraph(file);
            this.graphFilename = file.getName();
            if(!this.checkNames())
            {
                this.graph = null;
                throw new IncompatibleNamesException("Incompatible names");
            }
            else
            {
                this.statusGraph.setText("Graph: " + file.getName() + ".");
            }
        }

        if(this.graph != null && this.network != null)
            this.load.setText("Unload");
        this.inputPath.setDisable(this.network == null || this.graph == null);
        this.input.setDisable(this.network == null || this.graph == null);
    }

    private boolean checkNames()
    {
        if(this.networkFilename != null && this.graphFilename != null)
        {
            String net, gph;
            net = this.networkFilename.substring(0, this.networkFilename.length() - ".graphml".length());
            gph = this.graphFilename.substring(0, this.graphFilename.length() - ".graphml".length());
            if(net.endsWith("_network"))
                net = net.replaceAll("_network", "");
            if(gph.endsWith("_graph"))
                gph = gph.replaceAll("_graph", "");

            return net.equals(gph);
        }
        return true;
    }

    private Column[] getColumnsToSelect()
    {
        return new Column[] {
                Column.GRAPH_NODE_COUNT,
                Column.GRAPH_TRANSITION_COUNT,
                Column.GRAPH_UNIQUE_TRANSITION_COUNT,
                Column.NETWORK_NODE_COUNT,
                Column.NETWORK_TRANSITION_COUNT,
                Column.NETWORK_UNIQUE_TRANSITION_COUNT,
                Column.USED_NODE_COUNT,
                Column.UNUSED_NODE_COUNT,
                Column.USED_TRANSITION_COUNT,
                Column.UNUSED_TRANSITION_COUNT,
                Column.IS_REACHABLE,
                Column.ALL_PATHS
        };
    }
    
    private void styleTitleLabel()
    {
        this.title.setText("Explore");
        DoubleProperty fontSize = new SimpleDoubleProperty(0);
        fontSize.bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
        this.title.styleProperty().bind(Bindings.concat("-fx-font-weight: bold;-fx-font-size: ", fontSize.asString("%.0f")).concat("%;"));
        this.title.setTextAlignment(TextAlignment.CENTER);
    }

    private void styleStatusNetworkLabel()
    {
        this.statusNetwork.setText("Network: Not Loaded.");
    }

    private void styleStatusGraphLabel()
    {
        this.statusGraph.setText("Graph: Not Loaded.");
    }

    private void styleLoadDocumentComboBox()
    {
        this.loadDocument.getItems().addAll(Document.values());
        this.loadDocument.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.7));
        this.loadDocument.setValue(Document.DETERMINE);
    }

    private void styleColumnsToSelectListView()
    {
        this.columnsToSelect.getItems().addAll(this.getColumnsToSelect());
        this.columnsToSelect.minWidthProperty().bind(new SimpleIntegerProperty(200));
        this.columnsToSelect.prefWidthProperty().bind(
                Bindings.subtract(Bindings.multiply(primaryStage.widthProperty(), 0.5), 35)
        );
        this.columnsToSelect.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.columnsToSelect.setDisable(true);
    }

    private void styleSelectedColumnsListView()
    {
        this.selectedColumns.minWidthProperty().bind(new SimpleIntegerProperty(200));
        this.selectedColumns.prefWidthProperty().bind(
                Bindings.subtract(Bindings.multiply(primaryStage.widthProperty(), 0.5), 35)
        );
        this.selectedColumns.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.selectedColumns.setDisable(true);
    }

    private void styleLoadButton()
    {
        this.load.setText("Load");
        this.load.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));
        this.load.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));

        this.load.setOnMouseClicked(
                event ->
                {
                    if(this.load.getText().equals("Load"))
                    {
                        FileChooser fileChooser = this.getFileChooserGraphML();
                        File file = fileChooser.showOpenDialog(primaryStage);
                        if(file != null)
                        {
                            if(!file.isFile())
                            {
                                UIScene.showAlert("Please, select file", "Please, select file.", "Selected path does not lead to a file.", Alert.AlertType.INFORMATION);
                            }
                            else
                            {
                                try
                                {
                                    this.loadGraphs(file);

                                    this.loadControl = (this.network == null && this.graph == null);
                                    this.disableControls(this.inControl || this.loadControl);
                                }
                                catch(IOException | SAXException | ParserConfigurationException | NoSuchGraphType e)
                                {
                                    UIScene.showAlert("Task error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
                                }
                                catch(IncompatibleNamesException e)
                                {
                                    UIScene.showAlert("Incompatible names", "Graph and Network filenames are different.", "Graph and Network filenames should be mainly the same.\n" + "Ex.\n" + "AutomatonNetwork_network.grphml and\nAutomatonNetwork_graph.grphml.\n" + "Extension and '_network'|'_graph' ptart is ommited, the rest should match.", Alert.AlertType.ERROR);
                                }
                            }
                        }
                    }
                    else if(this.load.getText().equals("Unload"))
                    {
                        this.graph = null;
                        this.network = null;
                        this.graphFilename = null;
                        this.networkFilename = null;
                        this.statusGraph.setText("Graph: Not Loaded.");
                        this.statusNetwork.setText("Network: Not Loaded.");
                        this.loadControl = (this.network == null && this.graph == null);
                        this.disableControls(this.inControl || this.loadControl);
                        this.load.setText("Load");
                    }
                }
        );
    }

    private void styleSelectButton()
    {
        this.select.setText("->");
        this.select.setTextAlignment(TextAlignment.CENTER);
        this.select.minWidthProperty().bind(new SimpleIntegerProperty(35));
        this.select.setDisable(true);

        this.select.setOnMouseClicked(
                event ->
                {
                    ObservableList<Column> list = this.columnsToSelect.getSelectionModel().getSelectedItems();
                    List<Column> cp = new ArrayList<>();
                    for(Column column : list)
                        cp.add(column);
                    this.columnsToSelect.getItems().removeAll(cp);
                    this.selectedColumns.getItems().addAll(cp);
                    this.columnsToSelect.getSelectionModel().clearSelection();
                    this.columnsToSelect.refresh();
                    this.explore.setDisable(this.selectedColumns.getItems().isEmpty());
                }
        );
    }

    private void styleUnselectButton()
    {
        this.unselect.setText("<-");
        this.unselect.setTextAlignment(TextAlignment.CENTER);
        this.unselect.minWidthProperty().bind(new SimpleIntegerProperty(35));
        this.unselect.setDisable(true);

        this.unselect.setOnMouseClicked(
                event ->
                {
                    ObservableList<Column> list = this.selectedColumns.getSelectionModel().getSelectedItems();
                    List<Column> cp = new ArrayList<>();
                    for(Column column : list)
                        cp.add(column);
                    this.selectedColumns.getItems().removeAll(cp);
                    this.columnsToSelect.getItems().addAll(cp);
                    this.selectedColumns.getSelectionModel().clearSelection();
                    this.selectedColumns.refresh();
                    this.explore.setDisable(this.selectedColumns.getItems().isEmpty());
                }
        );
    }

    private void styleInputPathTextField()
    {
        this.inputPath.setPromptText("path/to/input/csv");
        this.inputPath.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.7));
        this.inputPath.setDisable(true);

        this.inputPath.textProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.inControl = newValue.trim().isEmpty();
                    this.disableControls(this.inControl || this.loadControl);
                }
        );
    }

    private void styleInputButton()
    {
        this.input.setText("Input");
        this.input.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));
        this.input.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));
        this.input.setDisable(true);

        this.input.setOnMouseClicked(
                event ->
                {
                    FileChooser fileChooser = this.getFileChooserCSV();
                    File file = fileChooser.showOpenDialog(primaryStage);
                    if(file != null)
                    {
                        if(!file.isFile())
                        {
                            UIScene.showAlert("Please, select file",
                                              "Please, select file.",
                                              "Selected path does not lead to a file.",
                                              Alert.AlertType.INFORMATION);
                        }
                        else
                        {
                            this.inputPath.setText(file.toString());
                            this.task = new ExploreTask(new File(this.inputPath.getText()));
                        }
                    }
                }
        );
    }

    private void styleWriteToCsvCheckBox()
    {
        this.writeToCsv.setText("Write to CSV");

        this.writeToCsv.selectedProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.columnsToSelect.setDisable(!newValue);
                    this.selectedColumns.setDisable(!newValue);
                    this.select.setDisable(!newValue);
                    this.unselect.setDisable(!newValue);
                }
        );
    }

    private void styleDrawCheckBox()
    {
        this.draw.setText("Draw");

        this.draw.setOnMouseClicked(
                event ->
                {
                    try
                    {
                        String dir = new File(this.inputPath.getText()).getParent();
                        if(this.network != null)
                            DrawUtils.draw(network, Paths.get(dir,
                                                              FileUtils.addSuffix(this.networkFilename, "network", "svg")).toFile());
                        if(this.graph != null)
                            DrawUtils.draw(graph, Paths.get(dir,
                                                            FileUtils.addSuffix(this.graphFilename, "graph", "svg")).toFile());
                    }
                    catch(IOException e)
                    {
                        UIScene.showAlert("Draw error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
                    }
                }
        );
    }

    private void styleDisplayData()
    {
        this.displayData.setText("Display\ndata");
        this.displayData.setTextAlignment(TextAlignment.CENTER);
        this.displayData.setMaxWidth(Double.MAX_VALUE);
        this.displayData.setMinWidth(10);
        this.displayData.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.5));
        this.displayData.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.displayData.setOnMouseClicked(
                event ->
                {
                    try
                    {
                        if(this.task != null)
                        {
                            CSVRow row = null;
                            if(this.graphFilename != null)
                                row = this.task.getRow(this.graphFilename);
                            else if(this.networkFilename != null)
                                row = this.task.getRow(this.networkFilename);

                            this.task.displayCSVRow(row);
                        }
                    }
                    catch(Exception e)
                    {
                        UIScene.showAlert("Task error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
                    }
                }
        );
    }

    private void styleExploreButton()
    {
        this.explore.setText("Explore");
        this.explore.setMaxWidth(Double.MAX_VALUE);
        this.explore.setMinWidth(10);
        this.explore.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.5));
        this.explore.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));
        this.explore.setDisable(true);

        this.explore.setOnMouseClicked(
                event ->
                {
                    try
                    {
                        CSVRow row = null;
                        if(this.graphFilename != null)
                            row = this.task.getRow(this.graphFilename);
                        else if(this.networkFilename != null)
                            row = this.task.getRow(this.networkFilename);

                        this.task.setColumns(this.selectedColumns.getItems());

                        this.task.execute(this.graph, this.network, row);
                    }
                    catch(Exception e)
                    {
                        UIScene.showAlert("Task error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
                    }
                }
        );
    }

    private void styleBackButton()
    {
        this.back.setText("Back");
        this.back.setMaxWidth(Double.MAX_VALUE);
        this.back.setMinWidth(10);
        this.back.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.5));
        this.back.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.back.setOnMouseClicked(
                event -> primaryStage.getScene().setRoot(startView)
        );
    }

    private void styleExitButton()
    {
        this.exit.setText("Exit");
        this.exit.setMaxWidth(Double.MAX_VALUE);
        this.exit.setMinWidth(10);
        this.exit.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.5));
        this.exit.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.exit.setOnMouseClicked(
                event -> Platform.exit()
        );
    }

    private void constraintGridPane(GridPane gridPane)
    {
        gridPane.setAlignment(Pos.CENTER);
        gridPane.hgapProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.02));
        gridPane.vgapProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.02));

        ColumnConstraints cc1 = new ColumnConstraints();
        cc1.setHgrow(Priority.ALWAYS);
        cc1.setFillWidth(true);
        cc1.setHalignment(HPos.CENTER);
        ColumnConstraints cc2 = new ColumnConstraints();
        cc2.setHgrow(Priority.ALWAYS);
        cc2.setFillWidth(true);
        cc2.setHalignment(HPos.CENTER);
        gridPane.getColumnConstraints().addAll(cc1, cc2);

        RowConstraints rc1 = new RowConstraints();
        rc1.setVgrow(Priority.ALWAYS);
        rc1.setFillHeight(true);
        rc1.setValignment(VPos.CENTER);
        RowConstraints rc2 = new RowConstraints();
        rc2.setVgrow(Priority.ALWAYS);
        rc2.setFillHeight(true);
        rc2.setValignment(VPos.CENTER);
        gridPane.getRowConstraints().addAll(rc1, rc2);
    }

    private void addElementsToGridPane()
    {
        this.gridPane.add(this.title, 0, 0, 2, 1);
        this.gridPane.add(this.statusNetwork, 0, 1, 2, 1);
        this.gridPane.add(this.statusGraph, 0, 2, 2, 1);
        this.gridPane.add(this.loadDocument, 0, 3);
        GridPane.setHalignment(this.loadDocument, HPos.LEFT);
        this.gridPane.add(this.load, 1, 3);
        GridPane.setHalignment(this.load, HPos.RIGHT);
        this.gridPane.add(this.inputPath, 0, 4);
        GridPane.setHalignment(this.inputPath, HPos.LEFT);
        this.gridPane.add(this.input, 1, 4);
        GridPane.setHalignment(this.input, HPos.RIGHT);
        this.gridPane.add(this.writeToCsv, 0, 5);
        GridPane.setHalignment(this.writeToCsv, HPos.LEFT);
        this.gridPane.add(this.draw, 1, 5);
        GridPane.setHalignment(this.draw, HPos.RIGHT);

        VBox vBox = new VBox(20, this.select, this.unselect);
        vBox.setAlignment(Pos.CENTER);
        HBox hBox = new HBox(this.columnsToSelect, vBox, this.selectedColumns);
        hBox.setAlignment(Pos.CENTER);
        this.gridPane.add(hBox, 0, 6, 2, 2);

        GridPane subGrid = new GridPane();
        constraintGridPane(subGrid);

        subGrid.add(this.explore, 0, 0);
        subGrid.add(this.displayData, 1, 0);
        subGrid.add(this.back, 0, 1);
        subGrid.add(this.exit, 1, 1);

        this.gridPane.add(subGrid, 0, 8, 2, 2);
    }

    private void constraintBorderPane()
    {
        this.borderPane.setCenter(this.gridPane);
        this.borderPane.setPadding(new Insets(40));
        this.borderPane.resizeRelocate(0, 0, Controller.WINDOW_WIDTH, Controller.WINDOW_HEIGHT);
    }

}
