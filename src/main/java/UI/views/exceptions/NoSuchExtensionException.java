package UI.views.exceptions;

public class NoSuchExtensionException extends Exception
{
    public NoSuchExtensionException()
    {
        super();
    }

    public NoSuchExtensionException(String message)
    {
        super(message);
    }
}
