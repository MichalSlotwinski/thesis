package UI.views.exceptions;

public class IncompatibleNamesException extends Exception
{
    public IncompatibleNamesException(String message)
    {
        super(message);
    }
}
