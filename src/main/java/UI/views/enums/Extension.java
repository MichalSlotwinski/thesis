package UI.views.enums;

import UI.views.exceptions.NoSuchExtensionException;

public enum Extension
{
    CUSTOM(".custom"),
    NTA(".nta"),
    XML(".graphml");

    private final String extension;

    Extension(String extension)
    {
        this.extension = extension;
    }

    public String getExtension() { return extension; }

    public static Extension determine(String filename) throws NoSuchExtensionException
    {
        if(filename.endsWith(".nta"))
        {
            return NTA;
        }
        else if(filename.endsWith(".custom"))
        {
            return CUSTOM;
        }
        else if(filename.endsWith(".graphml"))
        {
            return XML;
        }
        else
        {
            throw new NoSuchExtensionException("Unknown extension: " + filename);
        }
    }
}
