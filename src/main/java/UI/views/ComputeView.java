package UI.views;

import UI.Controller;
import UI.UIScene;
import UI.views.enums.BuildType;
import UI.views.enums.Depth;
import UI.views.enums.Extension;
import automaton.util.TrackMode;
import environment.Environment;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import task.ComputeTask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;


public class ComputeView implements UIScene
{
    private final Stage primaryStage;
    private Parent startView;
    private final Label title, selectStop1, selectStop2, coresLabel;
    private final ToggleGroup group;
    private final RadioButton selectFile, selectDir;
    private final ComboBox<Depth> depth;
    private final ComboBox<BuildType> buildType;
    private final Button input, output, solve, back, exit;
    private final TextField inputPath, outputPath, csvName;
    private final CheckBox exportXml, writeCsv, draw, writeLog;
    private final ComboBox<TrackMode> stopCondition1, stopCondition2;
    private final ComboBox<Extension> extensionFilter;
    private final Spinner<Integer> iterations1, iterations2;
    private final BorderPane borderPane;
    private final GridPane gridPane;
    private final Spinner<Integer> cores, logSpinner;

    private boolean inControl = true, outControl = true;

    public ComputeView(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
        this.gridPane = new GridPane();
        this.borderPane = new BorderPane();
        this.title = new Label();
        this.group = new ToggleGroup();
        this.selectFile = new RadioButton();
        this.selectDir = new RadioButton();
        this.depth = new ComboBox<>();
        this.buildType = new ComboBox<>();
        this.input = new Button();
        this.output = new Button();
        this.solve = new Button();
        this.back = new Button();
        this.exit = new Button();
        this.inputPath = new TextField();
        this.outputPath = new TextField();
        this.csvName = new TextField();
        this.exportXml = new CheckBox();
        this.coresLabel = new Label();
        this.writeCsv = new CheckBox();
        this.draw = new CheckBox();
        this.selectStop1 = new Label();
        this.selectStop2 = new Label();
        this.stopCondition1 = new ComboBox<>();
        this.stopCondition2 = new ComboBox<>();
        this.extensionFilter = new ComboBox<>();
        this.writeLog = new CheckBox();
        this.iterations1 = new Spinner<>();
        this.iterations2 = new Spinner<>();
        this.cores = new Spinner<>();
        this.logSpinner = new Spinner<>();
    }


    @Override
    public Parent makeView()
    {
        styleTitleLabel();
        styleSelectFileRadioBox();
        styleSelectDirRadioBox();
        styleExtensionFilterComboBox();
        styleDepthComboBox();
        styleBuildTypeComboBox();
        styleInputPathTextField();
        styleInputButton();
        styleOutputPathTextField();
        styleOutputButton();
        styleSolveButton();
        styleBackButton();
        styleExitButton();
        styleExportXmlCheckBox();
        styleWriteCsvCheckBox();
        styleDrawCheckBox();
        styleCsvNameTextField();
        styleCoresLabel();
        styleCoresSpinner();
        styleLoggerCheckbox();
        styleLoggerIntervalSpinner();
        styleStopCondition1ComboBox();
        styleSelectStop1Label();
        styleStopConditionIterations1Spinner();
        styleStopCondition2ComboBox();
        styleSelectStop2Label();
        styleStopConditionIterations2Spinner();
        constraintGridPane(this.gridPane);
        addElementsToGridPane();
        constraintBorderPane();
        disableControls(true);
        return this.borderPane;
    }

    public void setStartView(Parent startView) { this.startView = startView; }

    private void disableControls(boolean disable)
    {
        this.solve.setDisable(disable);
        this.exportXml.setDisable(disable);
        this.writeCsv.setDisable(disable);
        this.draw.setDisable(disable);
        this.coresLabel.setDisable(disable);
        this.cores.setDisable(disable);
        this.writeLog.setDisable(disable);
        this.stopCondition1.setDisable(disable);
        this.selectStop1.setDisable(disable);
        this.stopCondition2.setDisable(disable);
        this.selectStop2.setDisable(disable);
    }

    private FileChooser getFileChooser()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All files", "*.custom", "*.nta", "*.graphml"),
                new FileChooser.ExtensionFilter("Custom", "*.custom"),
                new FileChooser.ExtensionFilter("NTA", "*.nta"),
                new FileChooser.ExtensionFilter("GraphML", "*.graphml")
        );
        return fileChooser;
    }


    private void styleTitleLabel()
    {
        this.title.setText("Compute");
        DoubleProperty fontSize = new SimpleDoubleProperty(0);
        fontSize.bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
        this.title.styleProperty().bind(Bindings.concat("-fx-font-weight: bold;-fx-font-size: ", fontSize.asString("%.0f")).concat("%;"));
        this.title.setTextAlignment(TextAlignment.CENTER);
    }

    private void styleSelectFileRadioBox()
    {
        this.selectFile.setText("File");
        this.selectFile.setToggleGroup(this.group);
        this.selectFile.setSelected(false);
        this.selectFile.selectedProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.extensionFilter.setDisable(newValue);
                    this.inputPath.setText("");
                    this.depth.setDisable(newValue);
                }
        );
    }

    private void styleSelectDirRadioBox()
    {
        this.selectDir.setText("Directory");
        this.selectDir.setToggleGroup(this.group);
        this.selectDir.setSelected(true);
        this.selectDir.selectedProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.extensionFilter.setDisable(!newValue);
                    this.inputPath.setText("");
                    this.depth.setDisable(!newValue);
                }
        );
    }

    private void styleExtensionFilterComboBox()
    {
        this.extensionFilter.getItems().addAll(Extension.values());
        this.extensionFilter.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.2));
        this.extensionFilter.maxWidthProperty().bind(new SimpleIntegerProperty(100));
        this.extensionFilter.setValue(Extension.NTA);
    }

    private void styleDepthComboBox()
    {
        this.depth.getItems().addAll(Depth.values());
        this.depth.prefWidthProperty().bind(new SimpleIntegerProperty(130));
        this.depth.minWidthProperty().bind(new SimpleIntegerProperty(130));
        this.depth.setValue(Depth.RECURSIVE);
    }

    private void styleBuildTypeComboBox()
    {
        this.buildType.getItems().addAll(BuildType.values());
        this.buildType.prefWidthProperty().bind(new SimpleIntegerProperty(130));
        this.buildType.minWidthProperty().bind(new SimpleIntegerProperty(130));
        this.buildType.setValue(BuildType.EXECUTION);
    }

    private void styleInputPathTextField()
    {
        this.inputPath.setPromptText("path/to/input/resource...");
        this.inputPath.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.7));

        this.inputPath.textProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.inControl = newValue.trim().isEmpty();
                    this.disableControls(this.inControl || this.outControl);
                }
        );
    }

    private void styleInputButton()
    {
        this.input.setText("Input");
        this.input.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));
        this.input.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));

        this.input.setOnMouseClicked(
                event ->
                {
                    if(this.selectFile.isSelected())
                    {
                        FileChooser fileChooser = this.getFileChooser();
                        File file = fileChooser.showOpenDialog(primaryStage);
                        if(file != null)
                        {
                            if(!file.isFile())
                            {
                                UIScene.showAlert("Please, select file",
                                                  "Please, select file.",
                                                  "Selected path does not lead to a file.",
                                                  Alert.AlertType.INFORMATION);
                            }
                            else
                            {
                                this.inputPath.setText(file.toString());
                            }
                        }
                    }
                    else if(this.selectDir.isSelected())
                    {
                        DirectoryChooser directoryChooser = new DirectoryChooser();
                        File file = directoryChooser.showDialog(primaryStage);
                        if(file != null)
                        {
                            if(!file.isDirectory())
                            {
                                UIScene.showAlert("Please, select directory",
                                                  "Please, select directory.",
                                                  "Selected path does not lead to a directory.",
                                                  Alert.AlertType.INFORMATION);
                            }
                            else
                            {
                                this.inputPath.setText(file.toString());
                            }
                        }
                    }
                }
        );
    }

    private void styleOutputPathTextField()
    {
        this.outputPath.setPromptText("path/to/output/directory");
        this.outputPath.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.7));

        this.outputPath.textProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.outControl = newValue.trim().isEmpty();
                    this.disableControls(this.inControl || this.outControl);
                }
        );
    }

    private void styleOutputButton()
    {
        this.output.setText("Output");
        this.output.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));
        this.output.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.12));

        this.output.setOnMouseClicked(
                event ->
                {
                    DirectoryChooser directoryChooser = new DirectoryChooser();
                    File file = directoryChooser.showDialog(primaryStage);
                    if(file != null)
                    {
                        if(!file.isDirectory())
                        {
                            UIScene.showAlert("Please, select directory",
                                              "Please, select directory.",
                                              "Selected path does not lead to a directory.",
                                              Alert.AlertType.INFORMATION);
                        }

                        this.outputPath.setText(file.toString());
                    }
                }
        );
    }

    private void styleSolveButton()
    {
        this.solve.setText("Solve");
        this.solve.setMaxWidth(Double.MAX_VALUE);
        this.solve.setMinWidth(10);
        this.solve.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.7));
        this.solve.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.solve.setOnMouseClicked(
                event ->
                {
                    if(this.writeCsv.isSelected() && !this.csvName.getText().endsWith(".csv"))
                    {
                        UIScene.showAlert("Csv file name", "Csv file name is incorrect.", "Name of file must end with .csv extension.", Alert.AlertType.ERROR);
                    }
                    else
                    {
                        File input = Paths.get(this.inputPath.getText()).toFile();
                        File output = Paths.get(this.outputPath.getText()).toFile();

                        ComputeTask task = new ComputeTask(input, output);
                        task.setDir(this.selectDir.isSelected());
                        task.setExtension(this.extensionFilter.getValue());
                        task.setDepth(this.depth.getValue());
                        task.setBuildType(this.buildType.getValue());
                        task.setGraphml(this.exportXml.isSelected());
                        task.setCores(this.cores.getValue());
                        task.setCsv(this.writeCsv.isSelected());
                        task.setDraw(this.draw.isSelected());
                        task.setCsvName(this.csvName.getText());
                        task.setStopCondition1(this.stopCondition1.getValue());
                        task.setStopValue1(this.iterations1.getValue());
                        task.setStopCondition2(this.stopCondition2.getValue());
                        task.setStopValue2(this.iterations2.getValue());
                        task.setSnapThreshold(this.logSpinner.getValue());

                        try
                        {
                            task.execute();
                        }
                        catch(IOException e)
                        {
                            UIScene.showAlert("Task error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
                        }
                    }
                }
        );
    }

    private void styleBackButton()
    {
        this.back.setText("Back");
        this.back.setMaxWidth(Double.MAX_VALUE);
        this.back.setMinWidth(10);
        this.back.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.5));
        this.back.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.back.setOnMouseClicked(
                event -> primaryStage.getScene().setRoot(startView)
        );
    }

    private void styleExitButton()
    {
        this.exit.setText("Exit");
        this.exit.setMaxWidth(Double.MAX_VALUE);
        this.exit.setMinWidth(10);
        this.exit.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.5));
        this.exit.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.exit.setOnMouseClicked(
                event -> Platform.exit()
        );
    }

    private void styleExportXmlCheckBox()
    {
        this.exportXml.setText("Export to GraphML");
    }

    private void styleWriteCsvCheckBox()
    {
        this.writeCsv.setText("Write to CSV");
        this.writeCsv.selectedProperty().addListener(
                (observable, oldValue, newValue) -> this.csvName.setDisable(!newValue)
        );
    }

    private void styleDrawCheckBox()
    {
        this.draw.setText("Draw");
    }

    private void styleCsvNameTextField()
    {
        this.csvName.setText("result.csv");
        this.csvName.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
        this.csvName.setDisable(true);
    }

    private void styleCoresLabel()
    {
        this.coresLabel.setText("Select number of cores:");
        this.coresLabel.minWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
    }

    private void styleCoresSpinner()
    {
        int ac = Environment.getAvailableCores();
        SpinnerValueFactory.IntegerSpinnerValueFactory value = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, ac, 1);
        this.cores.setValueFactory(value);
        this.cores.setTooltip(new Tooltip("Set number of cores to be used."));
        this.cores.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.2));
        this.cores.maxWidthProperty().bind(new SimpleIntegerProperty(100));
        this.cores.setEditable(true);
    }

    private void styleLoggerCheckbox()
    {
        this.writeLog.setText("Write log");
        this.writeLog.selectedProperty().addListener(
                (observable, oldValue, newValue) -> this.logSpinner.setDisable(!newValue)
        );
    }

    private void styleLoggerIntervalSpinner()
    {
        SpinnerValueFactory.IntegerSpinnerValueFactory value = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 60, 1);
        this.logSpinner.setValueFactory(value);
        this.logSpinner.setTooltip(new Tooltip("Set interval (in seconds) by which to snapshot process state."));
        this.logSpinner.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.2));
        this.logSpinner.maxWidthProperty().bind(new SimpleIntegerProperty(100));
        this.logSpinner.setEditable(true);
        this.logSpinner.setDisable(true);
    }

    private void styleSelectStop1Label()
    {
        this.selectStop1.setText("Select stop condition 1:");
        this.selectStop1.minWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
    }

    private void styleStopCondition1ComboBox()
    {
        this.stopCondition1.getItems().addAll(TrackMode.TIME, TrackMode.OPERATIONS, TrackMode.NONE);
        this.stopCondition1.prefWidthProperty().bind(new SimpleIntegerProperty(130));
        this.stopCondition1.minWidthProperty().bind(new SimpleIntegerProperty(130));
        this.stopCondition1.setValue(TrackMode.NONE);

        this.stopCondition1.valueProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.iterations1.setDisable(newValue == TrackMode.NONE);

                    this.selectStop2.setDisable(newValue == TrackMode.OPERATIONS);
                    this.stopCondition2.setDisable(newValue == TrackMode.OPERATIONS);

                    if(newValue == TrackMode.TIME)
                    {
                        this.iterations1.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 150, 1));
                    }
                    else
                    {
                        this.iterations1.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 1000, 1));
                    }
                }
        );
    }

    private void styleStopConditionIterations1Spinner()
    {
        SpinnerValueFactory.IntegerSpinnerValueFactory value = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 1000, 1);
        this.iterations1.setValueFactory(value);
        this.iterations1.setTooltip(new Tooltip("Set max iterations or time (seconds) after witch to stop execution."));
        this.iterations1.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.2));
        this.iterations1.minWidthProperty().bind(new SimpleIntegerProperty(100));
        this.iterations1.setEditable(true);
        this.iterations1.setDisable(true);
    }

    private void styleSelectStop2Label()
    {
        this.selectStop2.setText("Select stop condition 2:");
        this.selectStop2.minWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
    }

    private void styleStopCondition2ComboBox()
    {
        this.stopCondition2.getItems().addAll(TrackMode.STATES, TrackMode.TRANSITIONS, TrackMode.NONE);
        this.stopCondition2.prefWidthProperty().bind(new SimpleIntegerProperty(130));
        this.stopCondition2.minWidthProperty().bind(new SimpleIntegerProperty(130));
        this.stopCondition2.setValue(TrackMode.NONE);

        this.stopCondition2.valueProperty().addListener(
                (observable, oldValue, newValue) ->
                {
                    this.iterations2.setDisable(newValue == TrackMode.NONE);
                    this.iterations2.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 1000, 1));
                }
        );
    }

    private void styleStopConditionIterations2Spinner()
    {
        SpinnerValueFactory.IntegerSpinnerValueFactory value = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 1000, 1);
        this.iterations2.setValueFactory(value);
        this.iterations2.setTooltip(new Tooltip("Set max iterations or time (seconds) after witch to stop execution."));
        this.iterations2.prefWidthProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.2));
        this.iterations2.minWidthProperty().bind(new SimpleIntegerProperty(100));
        this.iterations2.setEditable(true);
        this.iterations2.setDisable(true);
    }

    private void constraintGridPane(GridPane gridPane)
    {
        gridPane.setAlignment(Pos.CENTER);
        gridPane.hgapProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.02));
        gridPane.vgapProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.02));

        ColumnConstraints cc1 = new ColumnConstraints();
        cc1.setHgrow(Priority.ALWAYS);
        cc1.setFillWidth(true);
        cc1.setHalignment(HPos.CENTER);
        ColumnConstraints cc2 = new ColumnConstraints();
        cc2.setHgrow(Priority.ALWAYS);
        cc2.setFillWidth(true);
        cc2.setHalignment(HPos.CENTER);
        gridPane.getColumnConstraints().addAll(cc1, cc2);

        RowConstraints rc1 = new RowConstraints();
        rc1.setVgrow(Priority.ALWAYS);
        rc1.setFillHeight(true);
        rc1.setValignment(VPos.CENTER);
        RowConstraints rc2 = new RowConstraints();
        rc2.setVgrow(Priority.ALWAYS);
        rc2.setFillHeight(true);
        rc2.setValignment(VPos.CENTER);
        gridPane.getRowConstraints().addAll(rc1, rc2);
    }

    private void addElementsToGridPane()
    {
        this.gridPane.add(this.title, 0, 0, 5, 1);

        GridPane subGrid1 = new GridPane();
        constraintGridPane(subGrid1);
        subGrid1.add(this.selectDir, 0, 0);
        GridPane.setHalignment(this.selectDir, HPos.LEFT);
        subGrid1.add(this.extensionFilter, 1, 0);
        GridPane.setHalignment(this.extensionFilter, HPos.LEFT);
        subGrid1.add(this.buildType, 2, 0);
        GridPane.setHalignment(this.buildType, HPos.LEFT);
        subGrid1.add(this.selectFile, 0, 1);
        GridPane.setHalignment(this.selectFile, HPos.LEFT);
        subGrid1.add(this.depth, 1, 1);
        GridPane.setHalignment(this.depth, HPos.LEFT);
        this.gridPane.add(subGrid1, 0, 1, 5, 2);

        this.gridPane.add(this.inputPath, 0, 3, 3, 1);
        GridPane.setHalignment(this.inputPath, HPos.LEFT);
        this.gridPane.add(this.input, 4, 3);
        GridPane.setHalignment(this.input, HPos.RIGHT);
        this.gridPane.add(this.outputPath, 0, 4, 3, 1);
        GridPane.setHalignment(this.outputPath, HPos.LEFT);
        this.gridPane.add(this.output, 4, 4);
        GridPane.setHalignment(this.output, HPos.RIGHT);

        this.gridPane.add(this.writeCsv, 0, 5);
        GridPane.setHalignment(this.writeCsv, HPos.LEFT);
        this.gridPane.add(this.csvName, 2, 5);
        GridPane.setHalignment(this.csvName, HPos.CENTER);
        this.gridPane.add(this.draw, 4, 5);
        GridPane.setHalignment(this.draw, HPos.RIGHT);

        this.gridPane.add(this.exportXml, 0, 6);
        GridPane.setHalignment(this.exportXml, HPos.LEFT);
        this.gridPane.add(this.coresLabel, 2, 6);
        GridPane.setHalignment(this.coresLabel, HPos.CENTER);
        this.gridPane.add(this.cores, 4, 6);
        GridPane.setHalignment(this.cores, HPos.RIGHT);

        this.gridPane.add(this.writeLog, 0, 7);
        GridPane.setHalignment(this.writeLog, HPos.LEFT);
        this.gridPane.add(this.logSpinner, 2, 7);
        GridPane.setHalignment(this.logSpinner, HPos.LEFT);

        this.gridPane.add(this.selectStop1, 0, 8);
        GridPane.setHalignment(this.selectStop1, HPos.LEFT);
        this.gridPane.add(this.stopCondition1, 2, 8);
        GridPane.setHalignment(this.stopCondition1, HPos.CENTER);
        this.gridPane.add(this.iterations1, 4, 8);
        GridPane.setHalignment(this.iterations1, HPos.RIGHT);

        this.gridPane.add(this.selectStop2, 0, 9);
        GridPane.setHalignment(this.selectStop2, HPos.LEFT);
        this.gridPane.add(this.stopCondition2, 2, 9);
        GridPane.setHalignment(this.stopCondition2, HPos.CENTER);
        this.gridPane.add(this.iterations2, 4, 9);
        GridPane.setHalignment(this.iterations2, HPos.RIGHT);

        GridPane subGrid = new GridPane();
        constraintGridPane(subGrid);
        subGrid.add(this.solve, 0, 0, 2, 1);
        subGrid.add(this.back, 0, 1);
        subGrid.add(this.exit, 1, 1);

        this.gridPane.add(subGrid, 0, 10, 5, 2);
    }

    private void constraintBorderPane()
    {
        this.borderPane.setCenter(this.gridPane);
        this.borderPane.setPadding(new Insets(40));
        this.borderPane.resizeRelocate(0, 0, Controller.WINDOW_WIDTH, Controller.WINDOW_HEIGHT);
    }
}
