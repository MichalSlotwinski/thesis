package UI.views;

import UI.Controller;
import UI.UIScene;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class StartView implements UIScene
{
    private final Stage primaryStage;
    private Parent computeView, exploreView;
    private final GridPane gridPane;
    private final BorderPane borderPane;
    private final Button compute, explore, exit;
    private final Label title;

    public StartView(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
        this.gridPane = new GridPane();
        this.borderPane = new BorderPane();
        this.compute = new Button();
        this.explore = new Button();
        this.exit = new Button();
        this.title = new Label();
    }

    public Parent makeView()
    {
        styleTitleLabel();
        styleComputeButton();
        styleExploreButton();
        styleExitButton();
        constraintGridPane();
        addElementsToGridPane();
        constraintBorderPane();

        return this.borderPane;
    }

    public void setComputeView(Parent computeView) { this.computeView = computeView; }

    public void setExploreView(Parent exploreView) { this.exploreView = exploreView; }

    private void styleTitleLabel()
    {
        this.title.setText("Automaton Network to\nProduct Automaton");
        this.title.setFont(Font.font(25));
        DoubleProperty fontSize = new SimpleDoubleProperty(0);
        fontSize.bind(Bindings.multiply(primaryStage.widthProperty(), 0.3));
        this.title.styleProperty().bind(Bindings.concat("-fx-font-weight: bold;-fx-font-size: ", fontSize.asString("%.0f")).concat("%;"));
        this.title.setTextAlignment(TextAlignment.CENTER);
    }

    private void styleComputeButton()
    {
        this.compute.setText("Compute");
        this.compute.setMaxWidth(Double.MAX_VALUE);
        this.compute.setMinWidth(10);
        this.compute.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.35));
        this.compute.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.compute.setOnMouseClicked(
                event -> primaryStage.getScene().setRoot(computeView)
        );
    }

    private void styleExploreButton()
    {
        this.explore.setText("Explore");
        this.explore.setMaxWidth(Double.MAX_VALUE);
        this.explore.setMinWidth(10);
        this.explore.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.35));
        this.explore.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.explore.setOnMouseClicked(
                event -> primaryStage.getScene().setRoot(exploreView)
        );
    }

    private void styleExitButton()
    {
        this.exit.setText("Exit");
        this.exit.setMaxWidth(Double.MAX_VALUE);
        this.exit.setMinWidth(10);
        this.exit.prefWidthProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.7));
        this.exit.prefHeightProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.2));

        this.exit.setOnMouseClicked(
                event -> Platform.exit()
        );
    }

    private void constraintGridPane()
    {
        this.gridPane.setAlignment(Pos.CENTER);
        this.gridPane.hgapProperty().bind(Bindings.multiply(primaryStage.widthProperty(), 0.05));
        this.gridPane.vgapProperty().bind(Bindings.multiply(primaryStage.heightProperty(), 0.05));

        ColumnConstraints cc1 = new ColumnConstraints();
        cc1.setHgrow(Priority.ALWAYS);
        cc1.setFillWidth(true);
        cc1.setHalignment(HPos.CENTER);
        ColumnConstraints cc2 = new ColumnConstraints();
        cc2.setHgrow(Priority.ALWAYS);
        cc2.setFillWidth(true);
        cc2.setHalignment(HPos.CENTER);
        this.gridPane.getColumnConstraints().addAll(cc1, cc2);

        RowConstraints rc1 = new RowConstraints();
        rc1.setVgrow(Priority.ALWAYS);
        rc1.setFillHeight(true);
        rc1.setValignment(VPos.CENTER);
        RowConstraints rc2 = new RowConstraints();
        rc2.setVgrow(Priority.ALWAYS);
        rc2.setFillHeight(true);
        rc2.setValignment(VPos.CENTER);
        this.gridPane.getRowConstraints().addAll(rc1, rc2);
    }

    private void addElementsToGridPane()
    {
        this.gridPane.add(this.title, 0, 0, 2, 1);
        this.gridPane.add(this.compute, 0, 1);
        this.gridPane.add(this.explore, 1, 1);
        this.gridPane.add(this.exit, 0, 2, 2, 1);
    }

    private void constraintBorderPane()
    {
        this.borderPane.setCenter(this.gridPane);
        this.borderPane.setPadding(new Insets(40));
        this.borderPane.resizeRelocate(0, 0, Controller.WINDOW_WIDTH, Controller.WINDOW_HEIGHT);
    }
}
