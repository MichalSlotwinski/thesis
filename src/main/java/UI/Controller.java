package UI;

import UI.views.ComputeView;
import UI.views.ExploreView;
import UI.views.StartView;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Controller extends Application
{
    public static final int WINDOW_HEIGHT = 680;
    public static final int WINDOW_WIDTH = 520;
    public static final String TITLE = "Thesis program";

    StartView startView;
    ComputeView computeView;
    ExploreView exploreView;

    Scene scene;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        this.startView = new StartView(primaryStage);
        this.computeView = new ComputeView(primaryStage);
        this.exploreView = new ExploreView(primaryStage);

        this.startView.setComputeView(this.computeView.makeView());
        this.startView.setExploreView(this.exploreView.makeView());

        Parent start = this.startView.makeView();
        this.computeView.setStartView(start);
        this.exploreView.setStartView(start);


        primaryStage.setMinHeight(WINDOW_HEIGHT);
        primaryStage.setMinWidth(WINDOW_WIDTH);
        primaryStage.setMaxHeight(Screen.getPrimary().getVisualBounds().getHeight());
        primaryStage.setMaxWidth(Screen.getPrimary().getVisualBounds().getWidth());
        primaryStage.setTitle(TITLE);

        this.scene = new Scene(start, Controller.WINDOW_WIDTH, Controller.WINDOW_HEIGHT);
        primaryStage.setScene(this.scene);

        primaryStage.hide();
        primaryStage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
