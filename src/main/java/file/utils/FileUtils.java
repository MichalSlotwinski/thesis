package file.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtils
{
    public static String addSuffix(String filename, String suffix, String extension)
    {
        // split
        List<String> split = new ArrayList<>(Arrays.asList(filename.split("\\.")));
        // remove extension from list
        split.remove(split.size() - 1);
        // add suffix to name
        String name = String.join("", split);
        name += "_" + suffix;
        // add extension
        return name + "." + extension;
    }

    public static List<File> listFiles(File dir, boolean recursive, FileFilter filter)
    {
        if(!recursive)
        {
            File[] files = dir.listFiles(filter);
            return files != null ? Arrays.asList(files) : new ArrayList<>();
        }
        else
        {
            List<File> files = new ArrayList<>();
            listFilesRecursively(dir, files, filter);
            return files;
        }
    }

    private static void listFilesRecursively(File dir, List<File> fileList, FileFilter filter)
    {
        File[] files = dir.listFiles(filter);
        if(files != null)
        {
            for(File file : files)
            {
                if(file.isDirectory())
                    listFilesRecursively(file, fileList, filter);
                else if(file.isFile())
                    fileList.add(file);
            }
        }
    }
}
