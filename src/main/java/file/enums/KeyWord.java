package file.enums;

/**
 * Enum of key words in files to be parsed.
 * {@link file.parser.NetworkParser}
 */
public enum KeyWord
{
    NODE("LOCATION", "location"),
    TRANSITION("TRANSITIONS", "transition"),
    AUTOMATON("AUTOMATON_", "automaton"),
    NETWORK("NETWORK", "network"),
    END("END", "end");

    private final String custom;
    private final String nta;

    KeyWord(String custom, String nta)
    {
        this.custom = custom;
        this.nta = nta;
    }

    public String getCustom() { return custom; }

    public String getNta() { return nta; }
}
