package file.enums;

/**
 * Level enum that determines on which level parser currently is and which levels it can expect next.
 * {@link file.parser.NetworkParser}
 */
public enum Level
{
    NETWORK,
    AUTOMATON,
    NODE,
    TRANSITION,
    END,
    OTHER
}
