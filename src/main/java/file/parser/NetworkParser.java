package file.parser;

import automaton.model.Network;

import java.io.IOException;

public interface NetworkParser
{
    Network parse() throws IOException;
}
