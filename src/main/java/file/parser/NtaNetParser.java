package file.parser;

import automaton.model.Automaton;
import automaton.model.Network;
import automaton.model.Node;
import automaton.util.Triple;
import automaton.util.Tuple;
import file.enums.KeyWord;
import file.enums.Level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * Class implements parsing functionality for nta networks file.
 * Implements {@link NetworkParser} interface for standardized usage.
 */
public class NtaNetParser implements NetworkParser
{
    /**
     * End of section pattern.
     */
    private static final Pattern END_PATTERN = Pattern.compile("^#-*$");
    private Integer NODE_COUNTER = 0;

    private final File filepath;
    private Network network;
    private Automaton automaton;
    private Stack<Level> stack;
    private boolean nodeFlag;

    public NtaNetParser(File filepath)
    {
        this.filepath = filepath;
        this.network = null;
        this.automaton = null;
        this.stack = new Stack<>();
        this.nodeFlag = true; // indicates that file cursor is before first occurrence of node
    }

    /**
     * This method goes through file line by line and invokes {@code processLine(String)} method on given line.
     * @return Network object which represents parsed network file.
     * @throws IOException In case file access error.
     */
    @Override
    public Network parse() throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(this.filepath));
        for(String line; (line = reader.readLine()) != null; )
        {
            this.processLine(line, reader);
        }

        return this.network;
    }

    /**
     * This method determines which line is currently parsed and based on its result further operations are executed.
     * It determines semantic value based on line contents and pushes that value to stack (FIFO). This way on certain level
     * of file, certain levels can be expected.
     * @param line Single line from file.
     */
    private void determineLevel(String line)
    {
        if(!this.stack.empty())
            if(this.stack.peek() == Level.OTHER)
                this.stack.pop(); // OTHER

        if(line.contains(KeyWord.TRANSITION.getNta()) && this.stack.peek() == Level.AUTOMATON)
            this.stack.push(Level.TRANSITION);
        else if(line.contains(KeyWord.NODE.getNta()) && this.stack.peek() == Level.AUTOMATON)
            this.stack.push(Level.NODE);
        else if(END_PATTERN.matcher(line).find())
            this.stack.push(Level.END);
        else if(line.contains(KeyWord.AUTOMATON.getNta()) && this.stack.peek() == Level.NETWORK)
            this.stack.push(Level.AUTOMATON);
        else if(line.contains(KeyWord.NETWORK.getNta()) && this.stack.empty())
            this.stack.push(Level.NETWORK);
        else
            if(this.stack.peek() != Level.OTHER)
                this.stack.push(Level.OTHER);
    }

    /**
     * This method processes currently parsed line in file based on {@code determineLevel(String)} method.
     * Each possible {@link Level} is treated differently, which means that this method implements core mechanism of this
     * parser.
     * <ul>
     *     <li>If TRANSITION then add transition to current automaton</li>
     *     <li>Else If NODE then add node to current automaton</li>
     *     <li>Else If END then add current automaton to network</li>
     *     <li>Else If AUTOMATON then create current automaton</li>
     *     <li>Else If NETWORK then create network</li>
     * </ul>
     * @param line Single line from file.
     */
    private void processLine(String line, BufferedReader reader) throws IOException
    {
        this.determineLevel(line);

        if(this.stack.peek() == Level.TRANSITION)
        {
            // add transitions to automaton
            Triple<Integer, Integer, Integer> triple = this.getTransition(line);
            this.automaton.addTransition(triple.first(), triple.second(), triple.third());
            this.stack.pop(); //TRANSITION
        }
        else if(this.stack.peek() == Level.NODE)
        {
            // create nodes with ids
            this.automaton.addNode(new Node(++NODE_COUNTER));
            if(nodeFlag)
            {
                nodeFlag = false;
                this.automaton.setCurrent(NODE_COUNTER);
            }
            this.stack.pop(); // NODE
        }
        else if(this.stack.peek() == Level.END)
        {
            // add automaton to network
            this.stack.pop(); // END
            Automaton automaton = this.automaton;
            this.network.addAutomaton(automaton);
            this.stack.pop(); // AUTOMATON
            this.nodeFlag = true;
        }
        else if(this.stack.peek() == Level.AUTOMATON)
        {
            // create automaton with id
            line = reader.readLine();
            Integer automatonId = Integer.parseInt(line.trim().replaceAll("#", ""));
            this.automaton = new Automaton(automatonId);
        }
        else if(this.stack.peek() == Level.NETWORK)
        {
            // create network
            this.network = new Network();
        }
    }

    /**
     * This method is invoked when current line contains transition information.
     * It gets all three values representing transition in order and returns {@link Triple}:
     * <ol>
     *     <li>first - source node id</li>
     *     <li>second - destination node id</li>
     *     <li>third - transition id</li>
     * </ol>
     * @param line Single line from file which contains node information.
     * @return {@link Tuple} with first and last node id.
     */
    private Triple<Integer, Integer, Integer> getTransition(String line)
    {
        String[] split = line.trim().split(" ");
        return new Triple<Integer, Integer, Integer>(
                Integer.parseInt(split[1]),
                Integer.parseInt(split[2]),
                Integer.parseInt(split[3])
        );
    }
}
