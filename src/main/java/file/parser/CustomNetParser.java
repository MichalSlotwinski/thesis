package file.parser;

import automaton.model.Automaton;
import automaton.model.Network;
import automaton.model.Node;
import automaton.util.Triple;
import automaton.util.Tuple;
import file.enums.KeyWord;
import file.enums.Level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class implements parsing functionality for custom networks file.
 * Implements {@link NetworkParser} interface for standardized usage.
 */
public class CustomNetParser implements NetworkParser
{
    /**
     * Pattern for extracting first and last node id for currently parsed automaton.
     */
    private static final Pattern NODE_PATTERN = Pattern.compile(".*\\[(\\d+):(\\d+)]");

    private final File filepath;
    private Network network;
    private Automaton automaton;
    private Integer offset;
    private Stack<Level> stack;

    public CustomNetParser(File filepath)
    {
        this.filepath = filepath;
        this.network = null;
        this.automaton = null;
        this.offset = null;
        this.stack = new Stack<>();
    }

    /**
     * This method goes through file line by line and invokes {@code processLine(String)} method on given line.
     * @return Network object which represents parsed network file.
     * @throws IOException In case file access error.
     */
    @Override
    public Network parse() throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(this.filepath));
        for(String line; (line = reader.readLine()) != null; )
        {
            this.processLine(line);
        }

        return this.network;
    }

    /**
     * This method determines which line is currently parsed and based on its result further operations are executed.
     * It determines semantic value based on line contents and pushes that value to stack (FIFO). This way on certain level
     * of file, certain levels can be expected.
     * @param line Single line from file.
     * @return {@code true} if this line should be skipped (not parsed), {@code false} otherwise resulting in parsing that line.
     */
    private boolean determineLevel(String line)
    {
        if(line.contains(KeyWord.TRANSITION.getCustom()) && this.stack.peek() == Level.AUTOMATON)
        {
            this.stack.push(Level.TRANSITION);
            return true;
        }
        else if(line.contains(KeyWord.NODE.getCustom()) && this.stack.peek() == Level.AUTOMATON)
            this.stack.push(Level.NODE);
        else if(line.contains(KeyWord.END.getCustom()))
            this.stack.push(Level.END);
        else if(line.contains(KeyWord.AUTOMATON.getCustom()) && this.stack.peek() == Level.NETWORK)
            this.stack.push(Level.AUTOMATON);
        else if(line.contains(KeyWord.NETWORK.getCustom()) && this.stack.empty())
            this.stack.push(Level.NETWORK);

        return false;
    }

    /**
     * This method processes currently parsed line in file based on {@code determineLevel(String)} method.
     * Each possible {@link Level} is treated differently, which means that this method implements core mechanism of this
     * parser.
     * @param line Single line from file.
     */
    private void processLine(String line)
    {
        boolean skip = this.determineLevel(line);

        if(this.stack.peek() == Level.TRANSITION && !skip)
        {
            // add transitions to automaton
            Triple<Integer, Integer, Integer> triple = getTransition(line);
            this.automaton.addTransition(
                    triple.first() - this.offset,
                    triple.second() - this.offset,
                    triple.third());
        }
        else if(this.stack.peek() == Level.NODE)
        {
            // create nodes with ids
            Tuple<Integer, Integer> tuple = getLocations(line);
            this.offset = tuple.first();
            for(Integer i = tuple.first(); i <= tuple.second(); ++i)
                this.automaton.addNode(new Node(i));
            this.automaton.setCurrent(tuple.first());
            this.stack.pop();
        }
        else if(this.stack.peek() == Level.END)
        {
            // add automaton to network
            this.stack.pop(); // END
            if(this.stack.peek() != Level.NETWORK)
            {
                Automaton automaton = this.automaton;
                this.network.addAutomaton(automaton);
                this.stack.pop(); // TRANSITION
                this.stack.pop(); // AUTOMATON
            }
        }
        else if(this.stack.peek() == Level.AUTOMATON)
        {
            // create automaton with id
            String[] split = line.trim().split("_");
            Integer automatonId = Integer.parseInt(split[split.length - 1]);
            this.automaton = new Automaton(automatonId);
        }
        else if(this.stack.peek() == Level.NETWORK)
        {
            // create network
            this.network = new Network();
        }
    }

    /**
     * This method is invoked when current line contains node information. It gets first and last node id and returns a {@link Tuple} where:
     * <ol>
     *     <li>first - start node id</li>
     *     <li>second - last node id</li>
     * </ol>
     * @param line Single line from file which contains node information.
     * @return {@link Tuple} with first and last node id.
     */
    private Tuple<Integer, Integer> getLocations(String line)
    {
        Matcher m = NODE_PATTERN.matcher(line);
        m.find();
        Integer from = Integer.parseInt(m.group(1));
        Integer to = Integer.parseInt(m.group(2));

        return new Tuple<>(from, to);
    }

    /**
     * This method is invoked when current line contains transition information.
     * It gets all three values representing transition in order and returns {@link Triple}:
     * <ol>
     *     <li>first - source node id</li>
     *     <li>second - destination node id</li>
     *     <li>third - transition id</li>
     * </ol>
     * @param line Single line from file which contains node information.
     * @return {@link Tuple} with first and last node id.
     */
    private Triple<Integer, Integer, Integer> getTransition(String line)
    {
        String[] split = line.trim().split("_");
        return new Triple<>(
                Integer.parseInt(split[0]),
                Integer.parseInt(split[1]),
                Integer.parseInt(split[2])
        );
    }
}
