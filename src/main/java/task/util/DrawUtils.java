package task.util;

import automaton.model.Automaton;
import automaton.model.Graph;
import automaton.model.Neighbour;
import automaton.model.Network;
import guru.nidi.graphviz.attribute.Rank;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.Node;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class DrawUtils
{
    public static void draw(Graph graph, File file) throws IOException
    {
        guru.nidi.graphviz.model.Graph viz = Factory.graph()
                                                    .directed()
                                                    .graphAttr()
                                                    .with(Rank.dir(Rank.RankDir.TOP_TO_BOTTOM));

        Map<Integer, Node> map = new HashMap<>();
        for(automaton.model.Node node : graph.getNodes())
            map.put(node.getId(), Factory.node(node.getId().toString()));
        for(automaton.model.Node node : graph.getNodes())
        {
            Node vizNode = map.get(node.getId());
            for(Neighbour neighbour : node.getNeighbours())
            {
                vizNode = vizNode.link(Factory.to(map.get(neighbour.getNode().getId())).with(guru.nidi.graphviz.attribute.Label.of(neighbour.getTransitionId().toString())));
            }
            viz = viz.with(vizNode);
        }
        Graphviz.fromGraph(viz).totalMemory(Integer.MAX_VALUE - 7).render(Format.SVG).toFile(file);
    }

    public static void draw(Network network, File file) throws IOException
    {
        guru.nidi.graphviz.model.Graph viz = Factory.graph()
                                                    .directed()
                                                    .graphAttr()
                                                    .with(Rank.dir(Rank.RankDir.TOP_TO_BOTTOM));

        Map<Integer, guru.nidi.graphviz.model.Node> map = new HashMap<>();
        for(Automaton automaton : network.getAutomatons())
            for(automaton.model.Node node : automaton.getNodes())
                map.put(node.getId(), Factory.node(node.getId().toString()));
        for(Automaton automaton : network.getAutomatons())
        {
            for(automaton.model.Node node : automaton.getNodes())
            {
                guru.nidi.graphviz.model.Node vizNode = map.get(node.getId());
                for(Neighbour neighbour : node.getNeighbours())
                {
                    vizNode = vizNode.link(
                            Factory.to(map.get(neighbour.getNode().getId()))
                                   .with(guru.nidi.graphviz.attribute.Label.of(neighbour.getTransitionId().toString())));
                }
                viz = viz.with(vizNode);
            }
        }
        Graphviz.fromGraph(viz).totalMemory(Integer.MAX_VALUE-7).render(Format.SVG).toFile(file);
    }
}
