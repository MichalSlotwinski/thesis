package task;

import UI.views.enums.BuildType;
import UI.views.enums.Depth;
import UI.views.enums.Extension;
import automaton.algorithm.ExecutionBuilder;
import automaton.algorithm.ProductBuilder;
import automaton.algorithm.SemanticBuilder;
import automaton.algorithm.Statistic;
import automaton.model.*;
import automaton.util.TrackMode;
import automaton.util.Triple;
import environment.Environment;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import file.parser.NtaNetParser;
import file.utils.FileUtils;
import guru.nidi.graphviz.attribute.Rank;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Node;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.*;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.xml.sax.SAXException;
import storage.CSVExecLogger;
import storage.CSVMapper;
import storage.CSVRow;
import task.util.DrawUtils;
import xml.XmlExport;
import xml.XmlParser;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import guru.nidi.graphviz.model.Factory;

public class ComputeTask
{
    boolean dir = true,
            graphml = false,
            csv = false,
            draw = false;
    String csvName;
    Extension extension = Extension.NTA;
    Depth depth = Depth.RECURSIVE;
    BuildType buildType = BuildType.EXECUTION;
    File input, output;
    TrackMode stopCondition1 = TrackMode.NONE, stopCondition2 = TrackMode.NONE;
    Integer stopValue1, stopValue2, snapThreshold,
            cores = Environment.getAvailableCores();

    public ComputeTask(File input, File output)
    {
        this.input = input;
        this.output = output;
    }

    public void setDir(boolean dir) { this.dir = dir; }

    public void setGraphml(boolean graphml) { this.graphml = graphml; }

    public void setCsv(boolean csv) { this.csv = csv; }

    public void setDraw(boolean draw) { this.draw = draw; }

    public void setCsvName(String csvName) { this.csvName = csvName; }

    public void setExtension(Extension extension) { this.extension = extension; }

    public void setDepth(Depth depth) { this.depth = depth; }

    public void setBuildType(BuildType buildType) { this.buildType = buildType; }

    public void setStopCondition1(TrackMode stopCondition) { this.stopCondition1 = stopCondition; }

    public void setStopValue1(Integer stopValue) { this.stopValue1 = stopValue; }

    public void setStopCondition2(TrackMode stopCondition) { this.stopCondition2 = stopCondition; }

    public void setStopValue2(Integer stopValue) { this.stopValue2 = stopValue; }

    public void setSnapThreshold(Integer snapThreshold) { this.snapThreshold = snapThreshold; }

    public void setCores(Integer cores) { this.cores = cores; }

    public void execute() throws IOException
    {
        ExecutorService exec = Executors.newFixedThreadPool(this.cores, r -> {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t ;
        });

        BlockingQueue<File> files;
        CSVMapper mapper = new CSVMapper(Paths.get(this.output.toString(), this.csvName).toFile());
        if(this.dir)
        {
            files = new LinkedBlockingQueue<>(this.listFiles());
            if(this.csv)
            {
                mapper.header();
            }
        }
        else
        {
            files = new LinkedBlockingQueue<>();
            files.add(this.input);
        }

        Stage stage = new Stage();
        Label pendingTasksLabel = new Label();
        Label completion = new Label();
        Button button = new Button("Start");
        Button finish = new Button("Cancel");

        finish.setDisable(true);

        if(files.isEmpty())
        {
            finish.setText("Ok");
            finish.setDisable(false);
            button.setDisable(true);
        }
        TextArea log = new TextArea();
        log.setEditable(false);

        DoubleProperty progress = new SimpleDoubleProperty(0);

        ProgressBar progressBar = new ProgressBar();
        progressBar.prefWidthProperty().bind(new SimpleIntegerProperty(320));
        progressBar.progressProperty().bind(progress);

        IntegerProperty pendingTasks = new SimpleIntegerProperty(0);
        pendingTasksLabel.textProperty().bind(pendingTasks.asString("Pending Tasks: %d"));

        completion.textProperty().bind(Bindings.multiply(progress, files.size()).asString("%.0f/" + files.size()));

        finish.textProperty().bind(Bindings.when(pendingTasks.isEqualTo(0).and(button.disabledProperty())).then("Ok").otherwise("Cancel"));

        finish.setOnMouseClicked(
                event ->
                {
                    if(finish.getText().equals("Cancel"))
                    {
                        System.exit(0);
                        finish.setText("Ok");
                    }
                    else
                    {
                        shutdownAndAwaitTermination(exec);
                        stage.close();
                    }
                }
        );

        button.setOnAction(
                e ->
                {
                    button.setDisable(true);
                    finish.setDisable(false);
                    List<Task<String>> tasks = new ArrayList<>();
                    for(File file : files)
                        tasks.add(createTask(file, mapper));

                    // rebind progress:
                    progress.unbind();
                    progress.bind(new DoubleBinding()
                    {
                        {
                            for(Task<String> task : tasks)
                                bind(task.progressProperty());
                        }

                        @Override
                        public double computeValue()
                        {
                            return tasks.stream().mapToDouble(task -> Math.max(task.getProgress(), 0)).sum() / files.size();
                        }
                    });

                    log.appendText("Submitting tasks: " + files.size() + "\n");

                    pendingTasks.set(files.size());

                    // log state of each task:
                    tasks.forEach(task -> task.stateProperty().addListener((obs, oldState, newState) -> {
                        log.appendText(task.getMessage() + ": " + newState + "\n");

                        // update pendingTasks if task moves out of running state:
                        if(oldState == Worker.State.RUNNING)
                        {
                            pendingTasks.set(pendingTasks.get() - 1);
                        }
                    }));

                    tasks.forEach(exec::execute);
                });

        HBox buttons = new HBox(10, button, finish);
        HBox hBox = new HBox(10, progressBar, completion);
        VBox root = new VBox(10, pendingTasksLabel, hBox, log, buttons);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(10));
        VBox.setVgrow(log, Priority.ALWAYS);

        stage.setScene(new Scene(root, 400, 400));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    private Task<String> createTask(File file, CSVMapper mapper)
    {
        return new Task<String>() {
            {
                updateMessage(file.getName());
            }
            @Override
            public String call() throws Exception
            {
                updateProgress(0, 1);

                try
                {
                    Network network;
                    Extension ext = ComputeTask.this.dir ?
                                    ComputeTask.this.extension :
                                    Extension.determine(ComputeTask.this.input.getName());

                    switch(ext)
                    {
                        case NTA:
                            NetworkParser parserNta = new NtaNetParser(file);
                            network = parserNta.parse();
                            break;
                        case CUSTOM:
                            NetworkParser parserCustom = new CustomNetParser(file);
                            network = parserCustom.parse();
                            break;
                        case XML:
                            network = XmlParser.parseNetwork(file);
                            break;
                        default:
                            network = null;
                            break;
                    }

                    ProductBuilder builder;
                    switch(ComputeTask.this.buildType)
                    {
                        case EXECUTION:
                            if(ComputeTask.this.stopCondition1 != TrackMode.NONE || ComputeTask.this.stopCondition2 != TrackMode.NONE)
                                builder = new ExecutionBuilder(network, ComputeTask.this.stopCondition1, ComputeTask.this.stopValue1, ComputeTask.this.stopCondition2, ComputeTask.this.stopValue2, ComputeTask.this.snapThreshold);
                            else
                                builder = new ExecutionBuilder(network, ComputeTask.this.snapThreshold);
                            break;
                        case SEMANTIC:
                            if(ComputeTask.this.stopCondition1 != TrackMode.NONE || ComputeTask.this.stopCondition2 != TrackMode.NONE)
                                builder = new SemanticBuilder(network, ComputeTask.this.stopCondition1, ComputeTask.this.stopValue1, ComputeTask.this.stopCondition2, ComputeTask.this.stopValue2, ComputeTask.this.snapThreshold);
                            else
                                builder = new SemanticBuilder(network, ComputeTask.this.snapThreshold);
                            break;
                        default:
                            builder = null;
                    }

                    Graph graph = builder.build();
                    Statistic statistic = new Statistic(network, graph);

                    if(ComputeTask.this.csv)
                    {
                        if(ComputeTask.this.dir)
                        {
                            CSVRow row = new CSVRow(file, statistic, builder);
                            mapper.add(row);
                        }
                        else
                        {
                            if(mapper.getCsv().exists())
                            {
                                CSVRow row = mapper.get(ComputeTask.this.input.getName(), false);
                                CSVRow newRow = new CSVRow(ComputeTask.this.input, statistic, builder);
                                if(row != null)
                                {
                                    mapper.replace(ComputeTask.this.input.getName(), newRow);
                                }
                                else
                                {
                                    mapper.add(newRow);
                                }
                            }
                            else
                            {
                                mapper.header();
                                CSVRow row = new CSVRow(ComputeTask.this.input, statistic, builder);
                                mapper.add(row);
                            }
                        }
                    }

                    if(ComputeTask.this.snapThreshold != -1)
                    {
                        String logfile = FileUtils.addSuffix(file.getName(), "log", "csv");
                        CSVExecLogger logger = new CSVExecLogger(Paths.get(ComputeTask.this.output.toString(), logfile).toFile());
                        logger.header();
                        for(Triple<Long, Integer, Integer> snap : builder.getSnaps())
                        {
                            logger.add(snap.first(), snap.second(), snap.third());
                        }
                    }

                    if(ComputeTask.this.graphml)
                    {
                        XmlExport.export(network, Paths.get(ComputeTask.this.output.toString(),
                                                            FileUtils.addSuffix(file.getName(), "network", "graphml")).toFile());

                        XmlExport.export(graph, Paths.get(ComputeTask.this.output.toString(),
                                                          FileUtils.addSuffix(file.getName(), "graph", "graphml")).toFile());
                    }

                    if(ComputeTask.this.draw)
                    {
                        DrawUtils.draw(network, Paths.get(ComputeTask.this.output.toString(),
                                                          FileUtils.addSuffix(file.getName(), "network", "svg")).toFile());
                        DrawUtils.draw(graph, Paths.get(ComputeTask.this.output.toString(),
                                                        FileUtils.addSuffix(file.getName(), "graph", "svg")).toFile());
                    }
                }
                catch(IOException | ParserConfigurationException | SAXException | XMLStreamException e)
                {
                    e.printStackTrace();
                }

                updateProgress(1, 1);
                return null;
            }
        };
    }

    private void shutdownAndAwaitTermination(ExecutorService service)
    {
        service.shutdown(); // Disable new tasks from being submitted
        try
        {
            // Wait a while for existing tasks to terminate
            if (!service.awaitTermination(1, TimeUnit.SECONDS))
            {
                service.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!service.awaitTermination(1, TimeUnit.SECONDS))
                    System.err.println("Service did not terminate.");
            }
        }
        catch (InterruptedException ie)
        {
            // (Re-)Cancel if current thread also interrupted
            service.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    private List<File> listFiles()
    {
        if(this.depth == Depth.RECURSIVE)
        {
            return FileUtils.listFiles(this.input, true,
                                        pathname -> pathname.getName().endsWith(this.extension.getExtension())
                                                    || pathname.isDirectory()
            );
        }
        else
        {
            return FileUtils.listFiles(this.input, false,
                                        pathname -> pathname.getName().endsWith(this.extension.getExtension())
            );
        }
    }
}
