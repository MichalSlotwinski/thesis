package task;

import UI.UIScene;
import automaton.algorithm.Statistic;
import automaton.model.Graph;
import automaton.model.Network;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import storage.CSVMapper;
import storage.CSVRow;
import storage.enums.Column;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ExploreTask
{
    private File csvFile;
    private List<Column> columns;

    public ExploreTask(File csvFile)
    {
        this.csvFile = csvFile;
    }

    public void setColumns(List<Column> columns) { this.columns = columns; }

    public CSVRow getRow(String filename) throws IOException
    {
        filename = filename.substring(0, filename.length() - ".graphml".length());
        if(filename.endsWith("_network"))
            filename = filename.replaceAll("_network", "");
        else if(filename.endsWith("_graph"))
            filename = filename.replaceAll("_graph", "");

        return new CSVMapper(this.csvFile).get(filename, true);
    }

    public void displayCSVRow(CSVRow row) throws Exception
    {
        if(row != null)
        {
            Stage stage = new Stage();
            ObservableList<Row> list = FXCollections.observableArrayList();

            TableView<Row> tableView = new TableView<>();
            tableView.prefHeightProperty().bind(stage.heightProperty());
            tableView.prefWidthProperty().bind(stage.widthProperty());

            TableColumn<Row, String> column = new TableColumn<>("Column");
            column.setCellValueFactory(new PropertyValueFactory<>("column"));
            TableColumn<Row, String> value = new TableColumn<>("Value");
            value.setCellValueFactory(new PropertyValueFactory<>("value"));

            for(Column col : Column.values())
            {
                list.add(new Row(col.toString(), colToString(row, col)));
            }

            tableView.setItems(list);
            tableView.getColumns().add(column);
            tableView.getColumns().add(value);

            VBox vBox = new VBox(tableView);
            stage.setScene(new Scene(vBox, 400, 400));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        }
        else
        {
            throw new Exception("CSVRow was not initialized.");
        }
    }

    private static String colToString(CSVRow row, Column col) throws NoSuchFieldException
    {
        if(col == Column.ALL_PATHS)
            return row.allPathsToString();
        else if(col == Column.IS_REACHABLE)
            return row.isReachableToString();
        else if(col == Column.STOP_VALUE_1 || col == Column.STOP_VALUE_2 || col == Column.ACTUAL_VALUE)
        {
            Object obj = row.get(col);
            if(obj == null)
                return "";
            else
                return obj.toString();
        }
        else
            return row.get(col).toString();
    }

    public void execute(Graph graph, Network network, CSVRow row)
    {
        if(columns.contains(Column.ALL_PATHS) || columns.contains(Column.IS_REACHABLE))
        {
            Stage stage = new Stage();
            Label fromLabel = new Label("From");
            Label toLabel = new Label("To");
            Spinner<Integer> fromSpinner = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 1, 1));
            Spinner<Integer> toSpinner = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, 1, 1));
            fromSpinner.setEditable(true);
            toSpinner.setEditable(true);
            Button start = new Button("Start");
            Button cancel = new Button("Cancel");

            start.setOnMouseClicked(event -> {
                try
                {
                    executeRow(graph, network, row, fromSpinner.getValue(), toSpinner.getValue());
                }
                catch(Exception e)
                {
                    UIScene.showAlert("Task error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
                }
            });

            cancel.setOnMouseClicked(event -> stage.close());

            HBox from = new HBox(10, fromLabel, fromSpinner);
            from.setAlignment(Pos.CENTER);
            HBox to = new HBox(10, toLabel, toSpinner);
            to.setAlignment(Pos.CENTER);
            HBox buttons = new HBox(10, start, cancel);
            buttons.setAlignment(Pos.CENTER);
            HBox.setHgrow(fromLabel, Priority.ALWAYS);
            HBox.setHgrow(fromSpinner, Priority.ALWAYS);
            HBox.setHgrow(toLabel, Priority.ALWAYS);
            HBox.setHgrow(toSpinner, Priority.ALWAYS);
            HBox.setHgrow(start, Priority.ALWAYS);
            HBox.setHgrow(cancel, Priority.ALWAYS);
            VBox root = new VBox(10, from, to, buttons);
            root.setPadding(new Insets(10));
            stage.setResizable(false);
            stage.setScene(new Scene(root, 250, 120));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
            toLabel.setMinWidth(fromLabel.getWidth());
        }
        else
        {
            try
            {
                executeRow(graph, network, row);
            }
            catch(Exception e)
            {
                UIScene.showAlert("Task error", "There was an error during execution.", e.getMessage(), Alert.AlertType.ERROR);
            }
        }
    }

    public void executeRow(Graph graph, Network network, CSVRow row, Integer src, Integer dest) throws Exception
    {
        if(row != null)
        {
            Statistic statistic = new Statistic(network, graph);
            if(!statistic.getGraphNodes().contains(src) || !statistic.getGraphNodes().contains(dest))
                UIScene.showAlert("Indices error", "Node with given id does not exist.", "Please, select node id that exists in this graph.", Alert.AlertType.INFORMATION);
            else
            {
                CSVRow newRow = new CSVRow(statistic, row, columns, src, dest);
                String filename = (String) row.get(Column.FILENAME);
                CSVMapper mapper = new CSVMapper(csvFile);
                mapper.replace(filename, newRow);
                displayCSVRow(newRow);
            }
        }
        else
        {
            throw new Exception("CSVRow was not initialized.");
        }
    }

    public void executeRow(Graph graph, Network network, CSVRow row) throws Exception
    {
        if(row != null)
        {
            Statistic statistic = new Statistic(network, graph);
                CSVRow newRow = new CSVRow(statistic, row, columns, null, null);
                String filename = (String) row.get(Column.FILENAME);
                CSVMapper mapper = new CSVMapper(csvFile);
                mapper.replace(filename, newRow);
                displayCSVRow(newRow);
        }
        else
        {
            throw new Exception("CSVRow was not initialized.");
        }
    }

    public static class Row
    {
        public String column;
        public String value;

        public Row(String column, String value)
        {
            this.column = column;
            this.value = value;
        }

        public String getColumn() { return column; }
        public void setColumn(String column) { this.column = column; }
        public String getValue() { return value; }
        public void setValue(String value) { this.value = value; }
    }
}
