package automaton.util;

/**Enumeration of tracking modes:
 * <ol>
 *     <li>TRANSITIONS - tracks each transition check individually</li>
 *     <li>STATES - tracks each transition that did not contribute anything new to product automaton</li>
 *     <li>OPERATIONS - tracks operations of adding and checking nodes and transitions in product automaton</li>
 *     <li>TIME - tracks time of execution</li>
 *     <li>NONE - does not track progress</li>
 * </ol>*/
public enum TrackMode
{
    TRANSITIONS,
    STATES,
    OPERATIONS,
    TIME,
    NONE
}
