package automaton.util;

import java.util.*;

/**
 * Implements {@link Queue} interface and extends {@link LinkedList} implementation of it adding uniqueness of elements
 * in queue at given moment.
 * @param <E> Type of elements that this queue should store.
 */
public class UniqueQueue<E> extends LinkedList<E> implements Queue<E>
{
    private Integer maxIterations;
    /**Map of elements and it count. Element count is check by max iterations property.*/
    private Map<E, MutableInteger> blackList = new HashMap<>();
    private boolean track;

    public UniqueQueue()
    {
        super();
        this.maxIterations = null;
        this.track = false;
    }

    public UniqueQueue(Integer maxIterations)
    {
        super();
        this.maxIterations = maxIterations;
        this.track = true;
    }

    public UniqueQueue(Integer maxIterations, boolean trackAddition)
    {
        super();
        this.maxIterations = maxIterations;
        this.track = trackAddition;
    }

    /**
     * Increases count of given element in queue.
     * @param e Element which count should be incremented (increased by 1).
     */
    public void increase(E e)
    {
        MutableInteger value = this.blackList.get(e);
        if(value == null)
            this.blackList.put(e, new MutableInteger());
        else
            value.increment();
    }

    /**
     * Makes a check that returns if given element should be blocked or not. Blockage is based only on counter.
     * @param e Element to be checked for blockage.
     * @return @{true} only if element exists and its counter exceeded limit property.
     */
    private boolean isBlocked(E e)
    {
        MutableInteger value = this.blackList.get(e);
        return value != null && value.value > this.maxIterations;
    }

    @Override
    public boolean add(E e)
    {
        boolean block = this.isBlocked(e);
        // if tracking is enabled and element is not blocked
        // increment count
        if(this.track && !block)
            this.increase(e);
        // if it already is in queue or is blocked
        // don't add it
        if(this.contains(e) || block)
            return false;
        return super.add(e);
    }

    @Override
    public boolean offer(E e)
    {
        boolean block = this.isBlocked(e);
        // if tracking is enabled and element is not blocked
        // increment count
        if(this.track && !block)
            this.increase(e);
        // if it already is in queue or is blocked
        // don't add it
        if(this.contains(e))
            return false;
        return super.offer(e);
    }
}