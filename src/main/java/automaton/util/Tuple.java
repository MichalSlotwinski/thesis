package automaton.util;

import java.util.Objects;

public class Tuple<T1, T2>
{
    private T1 first;
    private T2 second;

    public Tuple(T1 first, T2 second)
    {
        this.first = first;
        this.second = second;
    }

    public T1 first() { return first; }
    public T2 second() { return second; }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return Objects.equals(first, tuple.first) &&
               Objects.equals(second, tuple.second);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(first, second);
    }

    @Override
    public String toString()
    {
        return "Tuple{" + "st=" + first + ", nd=" + second + '}';
    }
}
