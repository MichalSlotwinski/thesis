package automaton.util;

/**
 * Integer wrapper class for optimization purposes.
 */
public class MutableInteger
{
    Integer value = 1;
    public void increment() { ++value; }
    public Integer get() { return value; }
}
