package automaton.algorithm;

import UI.views.enums.BuildType;
import automaton.model.Graph;
import automaton.util.TrackMode;
import automaton.util.Triple;
import automaton.util.UniqueQueue;

import java.util.List;

/**
 * Interface for
 */
public interface ProductBuilder
{
    /**
     * <p>This method maps automaton network to product automaton. There are two implementations of this method;
     * one in {@link ExecutionBuilder} and one in {@link SemanticBuilder}.
     * {@link ExecutionBuilder} builds proper product automaton and {@link SemanticBuilder} builds product automaton of
     * all possible states combinations. Only difference between those two implementations is that {@link SemanticBuilder}
     * does not make transition check before execution, thus not filtering any. So by definition {@link ExecutionBuilder}
     * builds smaller automaton, and {@link SemanticBuilder} builds bigger automaton.</p>
     * <br/>
     * <p>Multiple stopping conditions were implemented:
     * <ul>
     *     <li>Number of individual transitions check limit</li>
     *     <li>Number of product automaton operations limit</li>
     *     <li>Number of transitions that did not add anything new to product automaton limit</li>
     *     <li>Time based</li>
     *     <li>Manual interruption</li>
     * </ul></p>
     * @return Graph instance of product automaton.
     */
    Graph build();

    /**
     * @param maxIterations Maximum iteration number to be considered depending on tracking mode.
     * @param mode Tracking mode determines for which purpose iteration limit is used.
     * @return {@link UniqueQueue} instance.
     */
    static UniqueQueue<Integer> getQueue(Integer maxIterations, TrackMode mode)
    {
        switch(mode)
        {
            case TRANSITIONS:
                if(maxIterations == null) throw new NullPointerException("Max iterations number is null.");
                return new UniqueQueue<>(maxIterations);
            case STATES:
                if(maxIterations == null) throw new NullPointerException("Max iterations number is null.");
                return new UniqueQueue<>(maxIterations, false);
            default: return new UniqueQueue<>();
        }
    }

    BuildType getType();

    TrackMode getTrackMode1();

    Integer getStopValue1();

    TrackMode getTrackMode2();

    Integer getStopValue2();

    Integer getActualStopValue();

    List<Triple<Long, Integer, Integer>> getSnaps();
}
