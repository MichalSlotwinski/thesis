package automaton.algorithm;

import automaton.model.*;

import java.util.*;
import java.util.stream.Collectors;

public class Statistic
{
    Network network;
    Graph productAutomaton;
    List<Integer> graphTransitions;
    List<Integer> uniqueGraphTransitions;
    List<Integer> networkTransitions;
    List<Integer> uniqueNetworkTransitions;
    List<Integer> usedNodes;
    List<Integer> unusedNodes;
    List<Integer> usedTransitions;
    List<Integer> unusedTransitions;

    public Statistic(Network network, Graph productAutomaton)
    {
        this.network = network;
        this.productAutomaton = productAutomaton;
        this.graphTransitions = this.graphTransitions();
        this.uniqueGraphTransitions = this.uniqueGraphTransitions();
        this.networkTransitions = this.networkTransitions();
        this.uniqueNetworkTransitions = this.uniqueNetworkTransitions();
        this.usedNodes = this.usedNodes();
        this.unusedNodes = this.unusedNodes();
        this.usedTransitions = this.usedTransitions();
        this.unusedTransitions = this.unusedTransitions();
    }

    public Integer countGraphNodes()
    {
        return this.productAutomaton.getNodes().size();
    }

    public Integer countGraphTransitions()
    {
        return this.graphTransitions.size();
    }

    public Integer countNetworkNodes()
    {
        int count = 0;
        for(Automaton automaton : network.getAutomatons())
            count += automaton.getNodes().size();
        return count;
    }

    public Integer countUniqueGraphTransitions()
    {
        return this.uniqueGraphTransitions.size();
    }

    public Integer countNetworkTransitions()
    {
        return this.networkTransitions.size();
    }

    public Integer countUniqueNetworkTransitions()
    {
        return this.uniqueNetworkTransitions.size();
    }

    public Integer countUsedNodes()
    {
        return this.usedNodes.size();
    }

    public Integer countUnusedNodes()
    {
        return this.unusedNodes.size();
    }

    public Integer countUsedTransitions()
    {
        return this.usedTransitions.size();
    }

    public Integer countUnusedTransitions()
    {
        return this.unusedTransitions.size();
    }

    public List<Integer> getGraphNodes()
    {
        return this.productAutomaton.getNodes()
                                    .stream()
                                    .map(Node::getId)
                                    .collect(Collectors.toList());
    }

    public List<Integer> getGraphTransitions()
    {
        return graphTransitions;
    }

    public List<Integer> getUniqueGraphTransitions()
    {
        return uniqueGraphTransitions;
    }

    public List<Integer> getNetworkNodes()
    {
        List<Node> nodes = new ArrayList<>();
        for(Automaton automaton : this.network.getAutomatons())
            nodes.addAll(automaton.getNodes());

        return nodes.stream()
                    .map(Node::getId)
                    .collect(Collectors.toList());
    }

    public List<Integer> getNetworkTransitions()
    {
        return networkTransitions;
    }

    public List<Integer> getUniqueNetworkTransitions()
    {
        return uniqueNetworkTransitions;
    }

    public List<Integer> getUsedNodes()
    {
        return usedNodes;
    }

    public List<Integer> getUnusedNodes()
    {
        return unusedNodes;
    }

    public List<Integer> getUsedTransitions()
    {
        return usedTransitions;
    }

    public List<Integer> getUnusedTransitions()
    {
        return unusedTransitions;
    }

    public boolean isReachable(Integer src, Integer dest)
    {
        Node from = this.productAutomaton.getNode(src);
        Node to = this.productAutomaton.getNode(dest);
        if(from.equals(to))
            return true;

        Stack<Node> stack = new Stack<>();
        stack.push(from);
        while(!stack.isEmpty())
        {
            Node node = stack.pop();
            for(Neighbour neighbour : node.getNeighbours())
            {
                if(to.equals(neighbour.getNode()))
                    return true;
                else
                    stack.push(neighbour.getNode());
            }
        }
        return false;
    }

    public List<List<Integer>> getAllPaths(Integer src, Integer dest)
    {
        boolean[] visited = new boolean[this.countGraphNodes()];
        Node start = productAutomaton.getNode(src);
        Node end = productAutomaton.getNode(dest);
        LinkedList<Node> path = new LinkedList<>();
        List<List<Integer>> paths = new ArrayList<>();

        path.add(start);
        visited[start.getId() - 1] = true;
        getAllPathsImpl(start, end, visited, path, paths);

        return paths;
    }




    private List<Integer> graphTransitions()
    {
        List<Integer> transitions = new ArrayList<>();
        for(Node node : this.productAutomaton.getNodes())
            for(Neighbour neighbour : node.getNeighbours())
                transitions.add(neighbour.getTransitionId());
        return transitions;
    }

    public static int getGraphTransitions(Graph graph)
    {
        List<Integer> transitions = new ArrayList<>();
        for(Node node : graph.getNodes())
            for(Neighbour neighbour : node.getNeighbours())
                transitions.add(neighbour.getTransitionId());
        return transitions.size();
    }

    private List<Integer> uniqueGraphTransitions()
    {
        Set<Integer> set = new HashSet<>(this.graphTransitions);
        return new ArrayList<Integer>(set);
    }

    private List<Integer> networkTransitions()
    {
        List<Integer> transitions = new ArrayList<>();
        for(Automaton automaton : this.network.getAutomatons())
            for(Node node : automaton.getNodes())
                for(Neighbour neighbour : node.getNeighbours())
                    transitions.add(neighbour.getTransitionId());
        return transitions;
    }

    private List<Integer> uniqueNetworkTransitions()
    {
        Set<Integer> set = new HashSet<>(this.networkTransitions);
        return new ArrayList<Integer>(set);
    }

    private List<Integer> usedTransitions()
    {
        Set<Integer> uniqueTransitions = new HashSet<>(this.graphTransitions);
        return new ArrayList<>(uniqueTransitions);
    }

    private List<Integer> unusedTransitions()
    {
        List<Integer> used = this.usedTransitions();
        Set<Integer> unused = new HashSet<>();
        for(Integer i : this.networkTransitions)
            if(!used.contains(i))
                unused.add(i);
        return new ArrayList<>(unused);
    }

    private List<Integer> usedNodes()
    {
        int nNodes = this.countNetworkNodes();
        Set<Integer> used = new HashSet<>();
        for(Node node : this.productAutomaton.getNodes())
        {
            if(used.size() == nNodes)
                return new ArrayList<>(used);
            else
                used.addAll(node.getState());
        }
        return new ArrayList<>(used);
    }

    private List<Integer> unusedNodes()
    {
        List<Integer> used = this.getUsedNodes();
        Set<Integer> unused = new HashSet<>();
        for(Integer i : this.getNetworkNodes())
            if(!used.contains(i))
                unused.add(i);
        return new ArrayList<>(unused);
    }

    private void getAllPathsImpl(Node start, Node end, boolean[] visited, LinkedList<Node> path, List<List<Integer>> paths)
    {
        if(start.equals(end))
        {
            paths.add(
                    new ArrayList<>(path)
                            .stream()
                            .map(Node::getId)
                            .collect(Collectors.toList())
            );
            Node node = path.removeLast();
            visited[node.getId() - 1] = false;
        }
        else
        {
            for(Neighbour neighbour : start.getNeighbours())
            {
                Node node = neighbour.getNode();
                if(!visited[node.getId() - 1])
                {
                    path.add(node);
                    visited[node.getId() - 1] = true;
                    getAllPathsImpl(node, end, visited, path, paths);
                }
            }
            path.removeLast();
            visited[start.getId() - 1] = false;
        }
    }
}
