package automaton.algorithm;

import UI.views.enums.BuildType;
import automaton.model.*;
import automaton.util.TrackMode;
import automaton.util.Triple;
import automaton.util.UniqueQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**Class represents a builder of proper product automaton.*/
public class ExecutionBuilder implements ProductBuilder
{
    /**Nodes unique identifier. Incremented each time new node is added to product automaton.*/
    private Integer NODE_COUNTER = 0;

    /**Incremented each time product automaton operation is executed.*/
    private Integer OPERATION_COUNTER = 0;

    Integer stopValue1, stopValue2;
    Network network;
    Graph productAutomaton;
    TrackMode mode1, mode2;
    List<Triple<Long, Integer, Integer>> snaps;
    Integer snapThreshold;
    private Integer actual;

    public ExecutionBuilder(Network network, Integer snapThreshold)
    {
        this.network = network;
        this.productAutomaton = null;
        this.stopValue1 = null;
        this.mode1 = TrackMode.NONE;
        this.stopValue2 = null;
        this.mode2 = TrackMode.NONE;
        this.snaps = new ArrayList<>();
        this.snapThreshold = (snapThreshold != -1) ? snapThreshold * 1000 : -1;
    }

    public ExecutionBuilder(Network network, TrackMode mode1, Integer stopValue1, TrackMode mode2, Integer stopValue2, Integer snapThreshold)
    {
        this.network = network;
        this.productAutomaton = null;
        this.stopValue1 = stopValue1;
        this.mode1 = mode1;
        this.stopValue2 = stopValue2;
        this.mode2 = mode2;
        this.snaps = new ArrayList<>();
        this.snapThreshold = (snapThreshold != -1) ? snapThreshold * 1000 : -1;

        if(this.mode1 == TrackMode.TIME)
            this.stopValue1 *= 1000;
    }

    @Override
    public BuildType getType() { return BuildType.EXECUTION; }

    @Override
    public List<Triple<Long, Integer, Integer>> getSnaps()
    {
        return this.snaps;
    }

    @Override
    public Integer getStopValue1()
    {
        if(this.mode1 == TrackMode.TIME)
            return stopValue1 / 1000;
        else
            return stopValue1;
    }

    @Override
    public TrackMode getTrackMode1() { return mode1; }

    @Override
    public Integer getStopValue2() { return stopValue2; }

    @Override
    public TrackMode getTrackMode2() { return mode2; }

    @Override
    public Integer getActualStopValue()
    {
        switch(this.mode1)
        {
            case TIME:
                return this.actual / 1000;
            case OPERATIONS:
                return OPERATION_COUNTER;
            default:
                return null;
        }
    }

    private long snap(long time, long snapStart, long snapStop)
    {
        if((snapStop - snapStart) >= this.snapThreshold)
        {
            this.snaps.add(
                    new Triple<>(time / 1000,
                                 this.productAutomaton.getNodes().size(),
                                 Statistic.getGraphTransitions(this.productAutomaton))
            );
            return System.currentTimeMillis();
        }
        return snapStart;
    }

    @Override
    public Graph build()
    {
        List<Integer> state;
        Stack<List<Integer>> stateStack = new Stack<>();
        this.productAutomaton = new Graph();

        // initial state
        state = this.network.getState();

        // add state to product automaton and stack
        this.productAutomaton.addNode(new Node(++NODE_COUNTER, state));
        stateStack.push(state);

        UniqueQueue<Integer> transitionQueue = ProductBuilder.getQueue(this.stopValue2, this.mode2);
        Integer transition;
        List<Integer> newState;

        // iterate over state
        // as long as there is state to check (in stack)
        long start = System.currentTimeMillis();
        long stop;
        long snapStart = System.currentTimeMillis();
        while(!stateStack.empty())
        {
            if(this.snapThreshold != -1)
                snapStart = this.snap(System.currentTimeMillis() - start,
                                      snapStart,
                                      System.currentTimeMillis());

            // if tracking operations is enabled
            // finish if max iterations are exceeded
            if(this.mode1 == TrackMode.OPERATIONS)
                if(OPERATION_COUNTER > this.stopValue1)
                    break;

            if(this.mode1 == TrackMode.TIME)
                if((stop = System.currentTimeMillis()) - start > this.stopValue1)
                {
                    this.actual = Math.toIntExact(stop - start);
                    break;
                }

            // get initial state to compute
            state = stateStack.pop();
            // set network to be in this state
            this.network.setState(state);

            // add every potential transition to queue
            // transitions are unique (doesn't add transition that is already present in queue)
            for(Automaton automaton : this.network.getAutomatons())
                for(Neighbour neighbour : automaton.getCurrent().getNeighbours())
                    transitionQueue.add(neighbour.getTransitionId());

            // iterate over transition
            // each transition is checked and executed thus building graph (product automaton).
            while(!transitionQueue.isEmpty())
            {
                if(this.mode1 == TrackMode.TIME)
                    if((stop = System.currentTimeMillis()) - start > this.stopValue1)
                    {
                        this.actual = Math.toIntExact(stop - start);
                        break;
                    }

                // get transition from queue and check if it can be executed
                transition = transitionQueue.poll();
                if(this.network.checkTransition(transition))
                {
                    // if it can be executed, execute it
                    // after that network is in new state
                    this.network.makeTransition(transition);
                    newState = this.network.getState();

                    // check if product automaton already contains that transition
                    // or if it contains new state
                    // if neither then add state and transition to it
                    if(productAutomaton.containsTransition(state, newState, transition))
                    {
                        if(this.mode2 == TrackMode.STATES) transitionQueue.increase(transition);
                        if(this.mode1 == TrackMode.OPERATIONS) ++OPERATION_COUNTER;
                    }
                    else if(productAutomaton.contains(newState))
                    {
                        productAutomaton.addTransition(state, newState, transition);
                        if(this.mode1 == TrackMode.OPERATIONS) ++OPERATION_COUNTER;
                    }
                    else
                    {
                        stateStack.push(newState);
                        productAutomaton.addNode(new Node(++NODE_COUNTER, newState));
                        productAutomaton.addTransition(state, newState, transition);
                        if(this.mode1 == TrackMode.OPERATIONS) ++OPERATION_COUNTER;
                    }
                    // after execution of transition, return to state before, to check another transition
                    this.network.setState(state);
                }
            }
        }

        if(this.mode1 == TrackMode.TIME)
            this.actual = Math.toIntExact(System.currentTimeMillis() - start);

        if(this.snapThreshold != -1)
            this.snap(System.currentTimeMillis() - start, 0, this.snapThreshold);

        return productAutomaton;
    }
}
