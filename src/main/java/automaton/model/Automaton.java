package automaton.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class represents a single element of {@link Network}.
 */
public class Automaton
{
    Integer id;
    List<Node> nodes;
    Node current;

    public Automaton(Integer id)
    {
        this.id = id;
        this.nodes = new ArrayList<>();
    }

    /**
     * Adds node to this automaton.
     * @param node Node to be added.
     */
    public void addNode(Node node)
    {
        this.nodes.add(node);
    }

    /**
     * Adds transition between two nodes. Firstly it gets those nodes by its identifiers and binds them by transition id.
     * @param sourceNodeId Source node from transition point of view.
     * @param destNodeId Destination node from transition point of view.
     * @param transitionId Transition identifier.
     */
    public void addTransition(Integer sourceNodeId, Integer destNodeId, Integer transitionId)
    {
        Node source = this.nodes.get(sourceNodeId);
        Node dest = this.nodes.get(destNodeId);
        source.addNeighbour(dest, transitionId);
    }

    /**
     * Finds node with given id and marks it as a current node int this automaton.
     * @param nodeId Node identifier.
     */
    public void setCurrent(Integer nodeId)
    {
        Integer startId = this.nodes.get(0).id;
        this.current = this.nodes.get(nodeId - startId);
    }

    /**
     * @return Id of this Automaton.
     */
    public Integer getId() { return id; }

    /**
     * @return Node marked as current.
     */
    public Node getCurrent() { return this.current; }

    /**
     * Checks whether transition with given id exists in this automaton.
     * @param transitionId Transition identifier.
     * @return {@code true} if transition exists, {@code false} otherwise.
     */
    public boolean transitionExists(Integer transitionId)
    {
        for(Node node : this.nodes)
            for(Neighbour neighbour : node.neighbours)
                if(neighbour.transitionId.equals(transitionId))
                    return true;
        return false;
    }

    /**
     * Checks whether given transition is adjacent to node marked as current int this automaton.
     * @param transitionId Transition id.
     * @return {@code true} if transition exists.
     */
    public boolean isTransitionAdjToCurrent(Integer transitionId)
    {
        for(Neighbour neighbour : this.current.neighbours)
            if(neighbour.transitionId.equals(transitionId))
                return true;
        return false;
    }

    /**
     * Executes transition in this automaton. It iterates through neighbours of current node in this automaton and based
     * on transition id makes that neighbouring node current node.
     * @param transitionId Transition identifier.
     */
    public void makeTransition(Integer transitionId)
    {
        for(Neighbour neighbour : this.current.neighbours)
            if(neighbour.transitionId.equals(transitionId))
                setCurrent(neighbour.node.id);
    }

    public List<Node> getNodes() { return nodes; }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Automaton automaton = (Automaton) o;
        return Objects.equals(id, automaton.id) && Objects.equals(nodes, automaton.nodes);
    }
}
