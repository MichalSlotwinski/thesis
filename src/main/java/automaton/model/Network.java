package automaton.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class represents network of automatons.
 */
public class Network
{
    List<Automaton> network;

    public Network()
    {
        this.network = new ArrayList<>();
    }

    /**
     * Adds single automaton to this network.
     * @param automaton {@link Automaton} to be added.
     */
    public void addAutomaton(Automaton automaton)
    {
        this.network.add(automaton);
    }

    public List<Automaton> getAutomatons() { return this.network; }

    /**
     * Collects identifier of each current node in each automaton in this network, and returns a list of states.
     * @return Current network state.
     */
    public List<Integer> getState()
    {
        List<Integer> state = new ArrayList<>();
        for(Automaton automaton : this.network)
            state.add(automaton.getCurrent().id);

        return state;
    }

    /**
     * Sets this network in given state. It goes through each automaton and sets corresponding node in each automaton
     * as current.
     * @param state Network state.
     */
    public void setState(List<Integer> state)
    {
        int i = 0;
        for(Automaton automaton : this.network)
            automaton.setCurrent(state.get(i++));
    }

    /**
     * Checks each automaton for given transition. It checks whether given transition can be executed.
     * Check is composed of two other checks.
     * <p>First check checks whether given transition exists in an automaton</p>
     * <p>Secondly if that transition exists then checks whether that transition is adjacent to current node in an automaton</p>
     * @param transitionId Transition identifier.
     * @return {@code true} if transition can be executed, {@code false} otherwise.
     */
    public boolean checkTransition(Integer transitionId)
    {
        for(Automaton automaton : this.network)
            if(automaton.transitionExists(transitionId))
                if(!automaton.isTransitionAdjToCurrent(transitionId))
                    return false;
        return true;
    }

    /**
     * Executes transition in each automaton.
     * @param transitionId Transition identifier.
     */
    public void makeTransition(Integer transitionId)
    {
        for(Automaton automaton : this.network)
            automaton.makeTransition(transitionId);
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Network network1 = (Network) o;
        return Objects.equals(network, network1.network);
    }
}
