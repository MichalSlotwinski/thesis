package automaton.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class represents graph structure of product automaton.
 */
public class Graph
{
    List<Node> nodes;

    public Graph()
    {
        this.nodes = new ArrayList<>();
    }

    /**
     * Adds single node to product automaton.
     * @param node {@link Node} to be added.
     */
    public void addNode(Node node)
    {
        this.nodes.add(node);
    }

    /**
     * Adds transition (directed edge) to product automaton. Firstly it gets node objects from states information form
     * product automaton. Secondly it binds them with given transition identifier.
     * @param src          State information of source node from transition point of view.
     * @param dest         State information of destination node from transition point of view.
     * @param transitionId Identifier of transition between given nodes.
     */
    public void addTransition(List<Integer> src, List<Integer> dest, Integer transitionId)
    {
        Node from = this.getNode(src);
        Node to = this.getNode(dest);
        from.addNeighbour(to, transitionId);
    }

    /**
     * Adds transition (directed edge) to product automaton. Firstly it gets node objects from id information form
     * product automaton. Secondly it binds them with given transition identifier.
     * @param src          Id of source node from transition point of view.
     * @param dest         Id of destination node from transition point of view.
     * @param transitionId Identifier of transition between given nodes.
     */
    public void addTransition(Integer src, Integer dest, Integer transitionId)
    {
        Node from = this.getNode(src);
        Node to = this.getNode(dest);
        from.addNeighbour(to, transitionId);
    }

    /**
     * Adds transition to product automaton. Firstly it checks if node with given states information exists in product automaton.
     * Secondly it checks if directed transition (edge) with given identifier already exists between them.
     * @param src          State information of source node from transition point of view.
     * @param dest         State information of destination node from transition point of view.
     * @param transitionId Identifier of transition between given nodes.
     */
    public boolean containsTransition(List<Integer> src, List<Integer> dest, Integer transitionId)
    {
        Node from = this.getNode(src);
        Node to = this.getNode(dest);
        if(to == null)
            return false;
        return from.hasNeighbour(new Neighbour(to, transitionId));
    }

    /**
     * Checks if node exists in product automaton with given state information.
     * @param state List of integers representing single state of automaton network.
     * @return {@code true} if given node exist in product automaton and is not {@code null}.
     */
    public boolean contains(List<Integer> state)
    {
        return this.getNode(state) != null;
    }

    /**
     * Returns node with given state information.
     * @param state List of integers representing single state of automaton network.
     * @return Single node instance of product automaton or {@code null} if doesn't exist.
     */
    public Node getNode(List<Integer> state)
    {
        for(Node node : this.nodes)
            if(node.state.size() == state.size() && node.state.equals(state))
                return node;

        return null;
    }

    /**
     * Returns node with given node identifier.
     * @param nodeId Node identifier.
     * @return Single node instance of product automaton or {@code null} if doesn't exist.
     */
    public Node getNode(Integer nodeId)
    {
        if(nodeId - 1 >= 0 && nodeId - 1 < this.nodes.size())
            return this.nodes.get(nodeId - 1);
        return null;
    }

    public List<Node> getNodes() { return nodes; }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Graph graph = (Graph) o;
        return Objects.equals(nodes, graph.nodes);
    }
}
