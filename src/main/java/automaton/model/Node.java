package automaton.model;

import java.util.ArrayList;
import java.util.List;

public class Node
{
    Integer id;
    List<Neighbour> neighbours;
    List<Integer> state;

    public Node(Integer id)
    {
        this.id = id;
        this.neighbours = new ArrayList<>();
        this.state = null;
    }

    public Node(Integer id, List<Integer> state)
    {
        this.id = id;
        this.neighbours = new ArrayList<>();
        this.state = state;
    }

    public List<Integer> getState() { return state; }

    public Integer getId() { return id; }

    /**
     * Adds neighbour to list of neighbours for this node.
     * @param node         Node to be added as a neighbour.
     * @param transitionId Identifier of transition binding those this node with given neighbour.
     */
    public void addNeighbour(Node node, Integer transitionId)
    {
        Neighbour neighbour = new Neighbour(node, transitionId);
        this.neighbours.add(neighbour);
    }

    /**
     * Checks weather this node has given node with given transition id as a neighbour.
     * @param neighbour Instance of {@link Neighbour} instance with Node and transition id.
     * @return {@code true} if this node has a given node as a neighbour with given transition id.
     */
    public boolean hasNeighbour(Neighbour neighbour)
    {
        for(Neighbour n : this.neighbours)
            if(n.equals(neighbour))
                return true;
        return false;
    }

    public List<Neighbour> getNeighbours()
    {
        return this.neighbours;
    }

    /**
     * Compares two nodes on states information only.
     * @param obj Node instance.
     * @return {@code true} if states are equal.
     */
    public boolean similar(Object obj)
    {
        if(obj instanceof Node)
        {
            Node node = (Node) obj;
            if(this.state != null && node.state != null)
                return this.state.equals(node.state);
            return this.state == node.state;
        }
        return false;
    }

    /**
     * Compares two nodes on states and id information.
     * @param obj Node instance.
     * @return {@code true} if states and id are equal.
     */
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Node)
        {
            Node node = (Node) obj;
            boolean id = this.id.equals(node.id);
            boolean state;
            if(this.state != null && node.state != null)
                state = this.state.equals(node.state);
            else
                state = this.state == node.state;
            return id && state;
        }
        return false;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[ID: ").append(id).append("]");
        if(neighbours != null)
            for(Neighbour neighbour : neighbours)
                sb.append(" -> [").append(neighbour.transitionId)
                  .append(" ").append(neighbour.node.id).append("]");

        return sb.toString();
    }
}
