package automaton.model;

public class Neighbour
{
    Node node;
    Integer transitionId;

    public Neighbour(Node node, Integer transitionId)
    {
        this.node = node;
        this.transitionId = transitionId;
    }

    public Integer getTransitionId() { return this.transitionId; }

    public Node getNode() { return node; }

    /**
     * Compares two neighbours on states information only.
     * @param obj Neighbour instance.
     * @return {@code true} if states are equal.
     */
    public boolean similar(Object obj)
    {
        if(obj instanceof Neighbour)
        {
            Neighbour neighbour = (Neighbour) obj;
            return this.node.similar(neighbour.node);
        }
        return false;
    }

    /**
     * Compares two neighbours on states and id information.
     * @param obj Neighbour instance.
     * @return {@code true} if states and id are equal.
     */
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Neighbour)
        {
            Neighbour neighbour = (Neighbour) obj;
            return this.transitionId.equals(neighbour.transitionId) && this.node.equals(neighbour.node);
        }
        return false;
    }
}
