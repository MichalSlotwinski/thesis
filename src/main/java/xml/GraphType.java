package xml;

public enum GraphType
{
    NETWORK("<!--network-->"),
    GRAPH("<!--graph-->"),
    UNKNOWN("");


    private final String type;

    GraphType(String type)
    {
        this.type = type;
    }

    public String getType() { return type; }
}
