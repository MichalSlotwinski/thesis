package xml.handler;

import automaton.model.Graph;
import automaton.model.Node;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import xml.enums.DataAttribute;
import xml.enums.ElementLevel;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class GraphHandler extends DefaultHandler
{
    private Graph graph;
    private NodeAdder nodeAdder;
    private EdgeAdder edgeAdder;
    private DataAttribute dataAttribute;
    private final Stack<ElementLevel> stack = new Stack<>();

    public Graph getGraph() { return graph; }

    private void determineLevel(String qName)
    {
        switch(qName)
        {
            case "graph":
                this.stack.push(ElementLevel.GRAPH);
                break;
            case "node":
                this.stack.push(ElementLevel.NODE);
                break;
            case "edge":
                this.stack.push(ElementLevel.EDGE);
                break;
            case "data":
                this.stack.push(ElementLevel.DATA);
                break;
        }
    }

    private void determineDataAttribute(String attribute)
    {
        switch(attribute)
        {
            case "nodeID":
                this.dataAttribute = DataAttribute.NODE_ID;
                break;
            case "nodeState":
                this.dataAttribute = DataAttribute.NODE_STATE;
                break;
            case "edgeID":
                this.dataAttribute = DataAttribute.EDGE_ID;
                break;
            default:
                this.dataAttribute = null;
                break;
        }
    }

    private List<Integer> parseList(String defaultListString)
    {
        defaultListString = defaultListString.replaceAll("(\\[|\\])", "").trim();
        String[] split = defaultListString.split(", ");
        return Arrays.stream(split)
                     .map(Integer::parseInt)
                     .collect(Collectors.toList());
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
    {
        determineLevel(qName);

        if(!this.stack.isEmpty())
        {
            if(this.stack.peek() == ElementLevel.GRAPH)
            {
                this.graph = new Graph();
            }
            else if(this.stack.peek() == ElementLevel.NODE)
            {
                this.nodeAdder = new NodeAdder();
            }
            else if(this.stack.peek() == ElementLevel.EDGE)
            {
                this.edgeAdder = new EdgeAdder();
                Integer src = Integer.parseInt(attributes.getValue("source").trim().replaceAll("n", ""));
                Integer dest = Integer.parseInt(attributes.getValue("target").trim().replaceAll("n", ""));
                this.edgeAdder.src = src;
                this.edgeAdder.dest = dest;
            }
            else if(this.stack.peek() == ElementLevel.DATA)
            {
                determineDataAttribute(attributes.getValue("key"));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName)
    {
        if(qName.equals("data"))
        {
            this.stack.pop();
        }
        else if(qName.equals("node"))
        {
            this.nodeAdder.addNodeToGraph(this.graph);
            this.nodeAdder = null;
            this.stack.pop();
        }
        else if(qName.equals("edge"))
        {
            this.edgeAdder.addEdgeToGraph(this.graph);
            this.edgeAdder = null;
            this.stack.pop();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
    {
        if(!this.stack.isEmpty())
        {
            if(this.stack.peek() == ElementLevel.DATA)
            {
                String characters = new String(ch, start, length);
                if(this.dataAttribute == DataAttribute.NODE_ID)
                {
                    this.nodeAdder.id = Integer.parseInt(characters);
                }
                else if(this.dataAttribute == DataAttribute.NODE_STATE)
                {
                    this.nodeAdder.state = parseList(characters);
                }
                else if(this.dataAttribute == DataAttribute.EDGE_ID)
                {
                    this.edgeAdder.id = Integer.parseInt(characters);
                }
            }
        }
    }

    private static class NodeAdder
    {
        private Integer id;
        private List<Integer> state;

        public void addNodeToGraph(Graph graph)
        {
            if(id != null && state != null)
                graph.addNode(new Node(id, state));
        }
    }

    private static class EdgeAdder
    {
        private Integer src;
        private Integer dest;
        private Integer id;

        public void addEdgeToGraph(Graph graph)
        {
            graph.addTransition(src, dest, id);
        }
    }
}
