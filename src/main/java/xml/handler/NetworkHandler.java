package xml.handler;

import automaton.model.Automaton;
import automaton.model.Network;
import automaton.model.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xml.enums.DataAttribute;
import xml.enums.ElementLevel;

import java.util.Collections;
import java.util.Comparator;
import java.util.Stack;

public class NetworkHandler extends DefaultHandler
{
    private Network network;
    private Automaton automaton;
    private NodeAdder nodeAdder;
    private EdgeAdder edgeAdder;
    private DataAttribute dataAttribute;
    private final Stack<ElementLevel> stack = new Stack<>();

    public Network getNetwork() { return network; }

    private void determineLevel(String qName)
    {
        switch(qName)
        {
            case "graph":
                this.stack.push(ElementLevel.GRAPH);
                break;
            case "node":
                this.stack.push(ElementLevel.NODE);
                break;
            case "edge":
                this.stack.push(ElementLevel.EDGE);
                break;
            case "data":
                this.stack.push(ElementLevel.DATA);
                break;
        }
    }

    private void determineDataAttribute(String attribute)
    {
        switch(attribute)
        {
            case "nodeID":
                this.dataAttribute = DataAttribute.NODE_ID;
                break;
            case "edgeID":
                this.dataAttribute = DataAttribute.EDGE_ID;
                break;
            default:
                this.dataAttribute = null;
                break;
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        determineLevel(qName);

        if(!this.stack.isEmpty())
        {
            if(this.stack.peek() == ElementLevel.GRAPH)
            {
                if(this.network == null)
                    this.network = new Network();

                Integer id = Integer.parseInt(attributes.getValue("id")
                                                                 .replaceAll("A", "")
                );
                this.automaton = new Automaton(id);
            }
            else if(this.stack.peek() == ElementLevel.NODE)
            {
                this.nodeAdder = new NodeAdder();
            }
            else if(this.stack.peek() == ElementLevel.EDGE)
            {
                this.edgeAdder = new EdgeAdder();
                Integer src = Integer.parseInt(attributes.getValue("source").trim().replaceAll("n", ""));
                Integer dest = Integer.parseInt(attributes.getValue("target").trim().replaceAll("n", ""));
                this.edgeAdder.src = src;
                this.edgeAdder.dest = dest;
            }
            else if(this.stack.peek() == ElementLevel.DATA)
            {
                determineDataAttribute(attributes.getValue("key"));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if(qName.equals("data"))
        {
            this.stack.pop();
        }
        else if(qName.equals("node"))
        {
            this.nodeAdder.addNodeToAutomaton(this.automaton);
            this.nodeAdder = null;
            this.stack.pop();
        }
        else if(qName.equals("edge"))
        {
            this.edgeAdder.addEdgeToAutomaton(this.automaton);
            this.edgeAdder = null;
            this.stack.pop();
        }
        else if(qName.equals("graph"))
        {
            Automaton automaton = this.automaton;
            this.network.addAutomaton(automaton);
            this.stack.pop();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
    {
        if(!this.stack.isEmpty())
        {
            if(this.stack.peek() == ElementLevel.DATA)
            {
                String characters = new String(ch, start, length);
                if(this.dataAttribute == DataAttribute.NODE_ID)
                {
                    this.nodeAdder.id = Integer.parseInt(characters);
                }
                else if(this.dataAttribute == DataAttribute.EDGE_ID)
                {
                    this.edgeAdder.id = Integer.parseInt(characters);
                }
            }
        }
    }

    private static class NodeAdder
    {
        Integer id;

        public void addNodeToAutomaton(Automaton automaton)
        {
            automaton.addNode(new Node(id));
        }
    }

    private static class EdgeAdder
    {
        Integer src;
        Integer dest;
        Integer id;

        public void addEdgeToAutomaton(Automaton automaton)
        {
            Integer offset = Collections.min(automaton.getNodes(), Comparator.comparing(Node::getId)).getId();
            automaton.addTransition(src - offset, dest - offset, id);
        }
    }
}
