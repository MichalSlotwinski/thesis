package xml.enums;

public enum DataAttribute
{
    NODE_ID,
    NODE_STATE,
    EDGE_ID
}
