package xml.enums;

public enum ElementLevel
{
    GRAPH,
    NODE,
    EDGE,
    DATA
}
