package xml;

import automaton.model.Automaton;
import automaton.model.Graph;
import automaton.model.Network;
import org.xml.sax.SAXException;
import xml.handler.GraphHandler;
import xml.handler.NetworkHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;

public abstract class XmlParser
{
    public static Graph parseGraph(File file) throws ParserConfigurationException, SAXException, IOException
    {
        GraphHandler handler = new GraphHandler();
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        parser.parse(file, handler);
        return handler.getGraph();
    }

    public static Network parseNetwork(File file) throws ParserConfigurationException, SAXException, IOException
    {
        NetworkHandler handler = new NetworkHandler();
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        parser.parse(file, handler);
        Network network = handler.getNetwork();
        for(Automaton automaton : network.getAutomatons())
            automaton.setCurrent(automaton.getNodes().get(0).getId());
        return network;
    }

    public static GraphType determineType(File file) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line; int i = 0;
        GraphType type = GraphType.UNKNOWN;
        while((line = reader.readLine()) != null && i < 5)
        {
            if(line.contains(GraphType.NETWORK.getType()))
            {
                type = GraphType.NETWORK;
                break;
            }
            else if(line.contains(GraphType.GRAPH.getType()))
            {
                type = GraphType.GRAPH;
                break;
            }
            i++;
        }
        return type;
    }
}
