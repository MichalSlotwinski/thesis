package xml.exceptions;

public class NoSuchGraphType extends Exception
{
    public NoSuchGraphType(String message)
    {
        super(message);
    }
}
