package xml;

import automaton.algorithm.ExecutionBuilder;
import automaton.algorithm.ProductBuilder;
import automaton.model.*;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public abstract class XmlExport
{
    public static void format(File file) throws ParserConfigurationException, IOException, SAXException, TransformerException
    {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = builder.parse(new InputSource(new BufferedInputStream(new FileInputStream(file))));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source source = new DOMSource(document);
        Result result = new StreamResult(new BufferedOutputStream(new FileOutputStream(file)));

        transformer.transform(source, result);
    }

    public static void export(Graph graph, File xml) throws XMLStreamException, FileNotFoundException
    {
        XMLStreamWriter stream = XMLOutputFactory
                .newInstance()
                .createXMLStreamWriter(new BufferedOutputStream(new FileOutputStream(xml)));

        stream.writeStartDocument("UTF-8", "1.0");
        stream.writeComment("graph");
        stream.writeStartElement("graphml");
        stream.writeAttribute("xmlns", "http://graphml.graphdrawing.org/xmlns");
        stream.writeAttribute("xmlns", "", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
        stream.writeAttribute("xsi", "", "schemaLocation", "http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");

        stream.writeStartElement("key");
        stream.writeAttribute("id", "nodeID");
        stream.writeAttribute("for", "node");
        stream.writeAttribute("attr.name", "id");
        stream.writeAttribute("attr.type", "int");
        stream.writeStartElement("default");
        stream.writeCharacters("-1");
        stream.writeEndElement(); // data
        stream.writeEndElement(); // key

        stream.writeStartElement("key");
        stream.writeAttribute("id", "nodeState");
        stream.writeAttribute("for", "node");
        stream.writeAttribute("attr.name", "state");
        stream.writeAttribute("attr.type", "string");
        stream.writeStartElement("default");
        stream.writeCharacters("[]");
        stream.writeEndElement(); // data
        stream.writeEndElement(); // key

        stream.writeStartElement("key");
        stream.writeAttribute("id", "edgeID");
        stream.writeAttribute("for", "edge");
        stream.writeAttribute("attr.name", "id");
        stream.writeAttribute("attr.type", "int");
        stream.writeStartElement("default");
        stream.writeCharacters("-1");
        stream.writeEndElement(); // data
        stream.writeEndElement(); // key

        stream.writeStartElement("graph");
        stream.writeAttribute("id", "G");
        stream.writeAttribute("edgedefault", "directed");
        stream.writeAttribute("parse.order", "nodesfirst");
        // first nodes
        for(Node node : graph.getNodes())
        {
            stream.writeStartElement("node");
            stream.writeAttribute("id", "n" + node.getId());
            stream.writeStartElement("data");
            stream.writeAttribute("key", "nodeID");
            stream.writeCharacters(node.getId().toString());
            stream.writeEndElement(); // data
            stream.writeStartElement("data");
            stream.writeAttribute("key", "nodeState");
            stream.writeCharacters(node.getState().toString());
            stream.writeEndElement(); // data
            stream.writeEndElement(); // node
        }
        // secondly edges
        for(Node node : graph.getNodes())
        {
            for(Neighbour neighbour : node.getNeighbours())
            {
                stream.writeStartElement("edge");
                stream.writeAttribute("source", "n" + node.getId());
                stream.writeAttribute("target", "n" + neighbour.getNode().getId());
                stream.writeStartElement("data");
                stream.writeAttribute("key", "edgeID");
                stream.writeCharacters(neighbour.getTransitionId().toString());
                stream.writeEndElement(); // data
                stream.writeEndElement(); // edge
            }
        }
        stream.writeEndElement(); //graph
        stream.writeEndDocument(); //graphml
        stream.close();
    }

    public static void export(Network network, File xml) throws FileNotFoundException, XMLStreamException
    {
        XMLStreamWriter stream = XMLOutputFactory
                .newInstance()
                .createXMLStreamWriter(new BufferedOutputStream(new FileOutputStream(xml)));

        stream.writeStartDocument("UTF-8", "1.0");
        stream.writeComment("network");
        stream.writeStartElement("graphml");
        stream.writeAttribute("xmlns", "http://graphml.graphdrawing.org/xmlns");
        stream.writeAttribute("xmlns", "", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
        stream.writeAttribute("xsi", "", "schemaLocation", "http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");

        stream.writeStartElement("key");
        stream.writeAttribute("id", "nodeID");
        stream.writeAttribute("for", "node");
        stream.writeAttribute("attr.name", "id");
        stream.writeAttribute("attr.type", "int");
        stream.writeStartElement("default");
        stream.writeCharacters("-1");
        stream.writeEndElement(); // default
        stream.writeEndElement(); // key

        stream.writeStartElement("key");
        stream.writeAttribute("id", "edgeID");
        stream.writeAttribute("for", "edge");
        stream.writeAttribute("attr.name", "id");
        stream.writeAttribute("attr.type", "int");
        stream.writeStartElement("default");
        stream.writeCharacters("-1");
        stream.writeEndElement(); // default
        stream.writeEndElement(); // key

        for(Automaton automaton : network.getAutomatons())
        {
            stream.writeStartElement("graph");
            stream.writeAttribute("id", "A" + automaton.getId());
            stream.writeAttribute("edgedefault", "directed");
            stream.writeAttribute("parse.order", "nodesfirst");
            // firstly nodes
            for(Node node : automaton.getNodes())
            {
                stream.writeStartElement("node");
                stream.writeAttribute("id", "n" + node.getId());
                stream.writeStartElement("data");
                stream.writeAttribute("key", "nodeID");
                stream.writeCharacters(node.getId().toString());
                stream.writeEndElement(); // data
                stream.writeEndElement(); // node

            }
            // secondly edges
            for(Node node : automaton.getNodes())
            {
                for(Neighbour neighbour : node.getNeighbours())
                {
                    stream.writeStartElement("edge");
                    stream.writeAttribute("source", "n" + node.getId());
                    stream.writeAttribute("target", "n" + neighbour.getNode().getId());
                    stream.writeStartElement("data");
                    stream.writeAttribute("key", "edgeID");
                    stream.writeCharacters(neighbour.getTransitionId().toString());
                    stream.writeEndElement(); // data
                    stream.writeEndElement(); // edge
                }
            }
            stream.writeEndElement(); // graph
        }
        stream.writeEndDocument(); //graphml
        stream.close();
    }
}
