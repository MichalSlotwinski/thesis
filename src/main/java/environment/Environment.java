package environment;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class Environment
{
    private static final int AVAILABLE_CORES;
    private static File TEMP_DIR = null;

    static
    {
        AVAILABLE_CORES = Runtime.getRuntime().availableProcessors();
        try
        {
            TEMP_DIR = Files.createTempDirectory(Paths.get(System.getProperty("java.io.tmpdir")), "").toFile();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static int getAvailableCores()
    {
        if(AVAILABLE_CORES <= 2)
            return 1;
        else
            return AVAILABLE_CORES - 2;
    }

    public static File getTempDir() { return TEMP_DIR; }
}
