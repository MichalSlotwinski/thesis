package xml;

import automaton.algorithm.ExecutionBuilder;
import automaton.algorithm.ProductBuilder;
import automaton.model.Graph;
import automaton.model.Network;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class XmlExportParserTest
{
    @Test
    public void exportParseNetworkTest() throws IOException, XMLStreamException, ParserConfigurationException, SAXException
    {
        File dir = new File("automatons/automaton_networks");
        for(File file : dir.listFiles(pathname -> pathname.getName().endsWith(".custom")))
        {
            NetworkParser parser = new CustomNetParser(file);
            Network network = parser.parse();
            File exportedNetwork = Paths.get("src/test/resources", file.getName() + ".graphml").toFile();
            XmlExport.export(network, exportedNetwork);
            Network test = XmlParser.parseNetwork(exportedNetwork);
            Assertions.assertEquals(network, test);
            exportedNetwork.delete();
        }
    }

    @Test
    public void exportParseGraphTest() throws IOException, XMLStreamException, ParserConfigurationException, SAXException
    {
        File dir = new File("automatons/automaton_networks");
        for(File file : dir.listFiles(pathname -> pathname.getName().endsWith(".custom")))
        {
            NetworkParser parser = new CustomNetParser(file);
            Network network = parser.parse();
            ProductBuilder builder = new ExecutionBuilder(network, -1);
            Graph graph = builder.build();
            File exportedGraph = Paths.get("src/test/resources", file.getName() + ".graphml").toFile();
            XmlExport.export(graph, exportedGraph);
            Graph test = XmlParser.parseGraph(exportedGraph);
            Assertions.assertEquals(graph, test);
            exportedGraph.delete();
        }
    }
}
