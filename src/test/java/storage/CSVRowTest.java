package storage;

import automaton.algorithm.ExecutionBuilder;
import automaton.algorithm.ProductBuilder;
import automaton.algorithm.Statistic;
import automaton.model.Graph;
import automaton.model.Network;
import automaton.util.TrackMode;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class CSVRowTest
{
    String networkPath = "automatons/automaton_networks/AutomatonNetwork.custom";
    String noReachAll = "\"AutomatonNetwork.custom\",\"EXECUTION\",\"21\",\"26\",\"9\",\"24\",\"31\",\"9\",\"24\",\"0\",\"9\",\"0\",\"NONE\",\"\",\"\",\"NONE\",\"\",\"\",\"\"";
    String withReachAll = "\"AutomatonNetwork.custom\",\"EXECUTION\",\"21\",\"26\",\"9\",\"24\",\"31\",\"9\",\"24\",\"0\",\"9\",\"0\",\"NONE\",\"\",\"\",\"NONE\",\"\",\"1->21:true|2->21:true|3->21:true\",\"1->21:{[1, 2, 3, 4, 5, 14, 15, 17, 19, 21],[1, 2, 3, 4, 5, 14, 16, 17, 19, 21],[1, 2, 3, 4, 5, 14, 16, 18, 19, 21],[1, 2, 3, 4, 5, 14, 16, 18, 20, 21]}|2->21:{[2, 3, 4, 5, 14, 15, 17, 19, 21],[2, 3, 4, 5, 14, 16, 17, 19, 21],[2, 3, 4, 5, 14, 16, 18, 19, 21],[2, 3, 4, 5, 14, 16, 18, 20, 21]}|3->21:{[3, 4, 5, 14, 15, 17, 19, 21],[3, 4, 5, 14, 16, 17, 19, 21],[3, 4, 5, 14, 16, 18, 19, 21],[3, 4, 5, 14, 16, 18, 20, 21]}\"";
    String tracked = "\"AutomatonNetwork.custom\",\"EXECUTION\",\"21\",\"26\",\"9\",\"24\",\"31\",\"9\",\"24\",\"0\",\"9\",\"0\",\"TIME\",\"150\",\"1\",\"STATES\",\"1000000\",\"\",\"\"";

    @Test
    public void test() throws IOException
    {
        File file = new File(networkPath);
        NetworkParser parser = new CustomNetParser(file);
        Network network = parser.parse();
        ProductBuilder builder = new ExecutionBuilder(network, -1);
        Graph graph = builder.build();
        Statistic statistic = new Statistic(network, graph);

        CSVRow row = new CSVRow(file, statistic, builder);
        CSVRow row3 = new CSVRow(noReachAll);
        Assertions.assertEquals(row3, row);

        row.addReachability(statistic, 1, 21);
        row.addPaths(statistic, 1, 21);
        row.addReachability(statistic, 2, 21);
        row.addPaths(statistic, 2, 21);

        NetworkParser parser1 = new CustomNetParser(file);
        Network network1 = parser1.parse();
        ProductBuilder builder1 = new ExecutionBuilder(network1, -1);
        Graph graph1 = builder1.build();
        Statistic statistic1 = new Statistic(network1, graph1);

        CSVRow row1 = new CSVRow(file, statistic1, builder1);
        row1.addReachability(statistic1, 1, 21);
        row1.addPaths(statistic1, 1, 21);
        row1.addReachability(statistic1, 2, 21);
        row1.addPaths(statistic1, 2, 21);
        row1.addReachability(statistic1, 3, 21);
        row1.addPaths(statistic1, 3, 21);
        Assertions.assertNotEquals(row, row1);

        CSVRow row2 = new CSVRow(withReachAll);
        Assertions.assertEquals(row2, row1);

        NetworkParser parser2 = new CustomNetParser(file);
        Network network2 = parser2.parse();
        ProductBuilder builder2 = new ExecutionBuilder(network2, TrackMode.TIME, 150, TrackMode.STATES, 1000000, -1);
        Graph graph2 = builder2.build();
        Statistic statistic2 = new Statistic(network2, graph2);

        CSVRow row4 = new CSVRow(file, statistic2, builder2);
        CSVRow row5 = new CSVRow(tracked);
        Assertions.assertEquals(row5, row4);
    }
}
