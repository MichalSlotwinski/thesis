package storage;

import automaton.algorithm.ExecutionBuilder;
import automaton.algorithm.ProductBuilder;
import automaton.algorithm.Statistic;
import automaton.model.Graph;
import automaton.model.Network;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CSVMapperTest
{
    String fullCSV = "src/test/resources/fullCSV.csv";
    String removed = "src/test/resources/removed.csv";
    String replaced = "src/test/resources/replaced.csv";

    String test = "src/test/resources/test.csv";

    @Test
    @Order(1)
    public void createCSV() throws IOException
    {
        File dir = new File("automatons/automaton_networks");
        File[] files = dir.listFiles(pathname -> pathname.getName().endsWith(".custom"));
        File testFile = new File(test);
        CSVMapper mapper = new CSVMapper(testFile);
        mapper.header();
        for(File file : files)
        {
            NetworkParser parser = new CustomNetParser(file);
            Network network = parser.parse();
            ProductBuilder builder = new ExecutionBuilder(network, -1);
            Graph graph = builder.build();
            Statistic statistic = new Statistic(network, graph);
            CSVRow row = new CSVRow(file, statistic, builder);
            mapper.add(row);
        }
        Assertions.assertEquals(Files.readAllLines(testFile.toPath()),
                                Files.readAllLines(Paths.get(fullCSV))
        );
    }

    @Test
    @Order(2)
    public void removeTest() throws IOException
    {
        File removedFile = new File(removed);
        File testFile = new File(test);
        CSVMapper mapper = new CSVMapper(testFile);
        mapper.remove("AutomatonNetwork.custom");
        Assertions.assertEquals(Files.readAllLines(removedFile.toPath()),
                                Files.readAllLines(testFile.toPath())
        );
    }

    @Test
    @Order(3)
    public void replaceTest() throws IOException
    {
        File replacedFile = new File(replaced);
        File testFile = new File(test);

        File file = new File("src/test/resources/AutomatonNetwork.custom");
        NetworkParser parser = new CustomNetParser(file);
        Network network = parser.parse();
        ProductBuilder builder = new ExecutionBuilder(network, -1);
        Graph graph = builder.build();
        Statistic statistic = new Statistic(network, graph);
        CSVRow row = new CSVRow(file, statistic, builder);

        CSVMapper mapper = new CSVMapper(testFile);
        mapper.replace("first_7.custom", row);
        Assertions.assertEquals(Files.readAllLines(replacedFile.toPath()),
                                Files.readAllLines(testFile.toPath())
        );
    }

    @AfterAll
    public static void cleanup()
    {
        File testFile = new File("src/test/resources/test.csv");
        testFile.delete();
    }
}
