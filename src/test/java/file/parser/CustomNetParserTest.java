package file.parser;

import automaton.model.Network;
import automaton.model.NetworkTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class CustomNetParserTest
{
    private CustomNetParser parser;
    private String filepath = "src/test/resources/AutomatonNetwork.custom";

    @BeforeEach
    public void setup()
    {
        File file = new File(this.filepath);
        this.parser = new CustomNetParser(file);
    }

    @Test
    public void parsedCustomNetworkTest()
    {
        try
        {
            Network network = parsedCustomNetworkBuild();
            NetworkTest.checkNetwork(network);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private Network parsedCustomNetworkBuild() throws IOException
    {
        File file = new File(this.filepath);
        this.parser = new CustomNetParser(file);
        return parser.parse();
    }
}
