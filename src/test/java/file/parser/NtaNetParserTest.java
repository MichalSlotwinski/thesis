package file.parser;

import automaton.model.Network;
import automaton.model.NetworkTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class NtaNetParserTest
{
    private NtaNetParser parser;
    private String filepath = "src/test/resources/AutomatonNetwork.nta";

    @BeforeEach
    public void setup()
    {
        File file = new File(this.filepath);
        this.parser = new NtaNetParser(file);
    }

    @Test
    public void parsedCustomNetworkTest()
    {
        try
        {
            Network network = parsedNtaNetworkBuild();
            NetworkTest.checkNetwork(network);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private Network parsedNtaNetworkBuild() throws IOException
    {
        File file = new File(this.filepath);
        this.parser = new NtaNetParser(file);
        return parser.parse();
    }
}
