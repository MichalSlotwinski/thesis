package automaton.algorithm;

import automaton.model.Graph;
import automaton.model.GraphTest;
import automaton.model.Network;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import file.parser.NtaNetParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class ExecutionBuilderTest
{
    private final String customFilePath = "src/test/resources/AutomatonNetwork.custom";
    private final String ntaFilePath = "src/test/resources/AutomatonNetwork.nta";
    private Network customNetwork;
    private Network ntaNetwork;

    @BeforeEach
    public void setup()
    {
        try
        {
            File file = new File(this.customFilePath);
            NetworkParser parser = new CustomNetParser(file);
            this.customNetwork = parser.parse();

            file = new File(this.ntaFilePath);
            parser = new NtaNetParser(file);
            this.ntaNetwork = parser.parse();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void customExecutionBuildGraphTest()
    {
        ProductBuilder builder = new ExecutionBuilder(this.customNetwork, 60000);
        Graph productAutomaton = builder.build();
        GraphTest.checkGraph(productAutomaton);
    }

    @Test
    public void ntaExecutionBuildGraphTest()
    {
        ProductBuilder builder = new ExecutionBuilder(this.ntaNetwork, 60000);
        Graph productAutomaton = builder.build();
        GraphTest.checkGraph(productAutomaton);
    }
}
