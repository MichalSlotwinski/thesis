package automaton.algorithm;

import automaton.model.Graph;
import automaton.model.Network;
import file.parser.CustomNetParser;
import file.parser.NetworkParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class StatisticTest
{
    @Test
    public void statisticTest()
    {
        try
        {
            File file = new File("src/test/resources/AutomatonNetwork.custom");
            NetworkParser parser = new CustomNetParser(file);
            Network network = parser.parse();
            ProductBuilder builder = new ExecutionBuilder(network, -1);
            Graph productAutomaton = builder.build();
            
            Statistic stats = new Statistic(network, productAutomaton);
            
            Assertions.assertEquals(21, stats.countGraphNodes());
            Assertions.assertEquals(26, stats.countGraphTransitions());
            Assertions.assertEquals(9, stats.countUniqueGraphTransitions());

            Assertions.assertEquals(24, stats.countNetworkNodes());
            Assertions.assertEquals(31, stats.countNetworkTransitions());
            Assertions.assertEquals(9, stats.countUniqueNetworkTransitions());

            Assertions.assertEquals(24, stats.countUsedNodes());
            Assertions.assertEquals(0, stats.countUnusedNodes());
            Assertions.assertTrue(stats.getUnusedNodes().isEmpty());

            Assertions.assertEquals(9, stats.countUsedTransitions());
            Assertions.assertEquals(0, stats.countUnusedTransitions());
            Assertions.assertTrue(stats.getUnusedTransitions().isEmpty());

            Integer[] a2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};
            Assertions.assertEquals(Arrays.asList(a2), stats.getGraphNodes());

            Integer[] a3 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assertions.assertTrue(stats.getGraphTransitions().containsAll(Arrays.asList(a3)));
            Assertions.assertTrue(stats.getUniqueGraphTransitions().containsAll(Arrays.asList(a3)));

            Integer[] a4 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
            Assertions.assertEquals(Arrays.asList(a4), stats.getNetworkNodes());

            Assertions.assertTrue(stats.getNetworkTransitions().containsAll(Arrays.asList(a3)));
            Assertions.assertTrue(stats.getUniqueNetworkTransitions().containsAll(Arrays.asList(a3)));

            Assertions.assertEquals(Arrays.asList(a4), stats.getUsedNodes());

            Assertions.assertTrue(stats.getUnusedNodes().isEmpty());

            Assertions.assertEquals(Arrays.asList(a3), stats.getUsedTransitions());

            Assertions.assertTrue(stats.getUnusedTransitions().isEmpty());

            Assertions.assertTrue(stats.isReachable(1, 21));
            Assertions.assertFalse(stats.isReachable(6, 14));

            Integer[] p1 = {1, 2, 3, 4, 5, 14, 15, 17, 19, 21};
            Integer[] p2 = {1, 2, 3, 4, 5, 14, 16, 17, 19, 21};
            Integer[] p3 = {1, 2, 3, 4, 5, 14, 16, 18, 19, 21};
            Integer[] p4 = {1, 2, 3, 4, 5, 14, 16, 18, 20, 21};
            List<List<Integer>> paths = stats.getAllPaths(1, 21);
            Assertions.assertEquals(4, paths.size());
            Assertions.assertTrue(paths.contains(Arrays.asList(p1)));
            Assertions.assertTrue(paths.contains(Arrays.asList(p2)));
            Assertions.assertTrue(paths.contains(Arrays.asList(p3)));
            Assertions.assertTrue(paths.contains(Arrays.asList(p4)));
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    public static void main(String[] args)
    {
        File file = new File(StatisticTest.class.getClassLoader().getResource("AutomatonNetwork.custom").getFile());
        System.out.println(file.toString());

    }
}
