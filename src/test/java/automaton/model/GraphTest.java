package automaton.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GraphTest
{
    @Test
    public void manualGraphBuildTest()
    {
        Graph graph = GraphTest.manualBuildExecutionGraph();
        GraphTest.checkGraph(graph);
    }

    public static void checkGraph(Graph productAutomaton)
    {
        // check number of nodes
        Assertions.assertEquals(21, productAutomaton.nodes.size());

        //check number of transitions
        int nTransition = 0;
        Set<Integer> uniqueTransitions = new HashSet<>();
        for(Node node : productAutomaton.nodes)
        {
            for(Neighbour neighbour : node.neighbours)
                uniqueTransitions.add(neighbour.transitionId);
            nTransition += node.neighbours.size();
        }
        Assertions.assertEquals(26, nTransition);
        Assertions.assertEquals(9, uniqueTransitions.size());

        // check nodes
        Assertions.assertTrue(productAutomaton.contains(state(1, 5, 9 , 13, 15, 17, 19, 21, 23)));
        Assertions.assertTrue(productAutomaton.contains(state(2, 5, 9 , 14, 16, 17, 19, 21, 23)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 5, 9 , 14, 16, 17, 20, 21, 23)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 6, 9 , 14, 16, 17, 20, 21, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 6, 9 , 14, 16, 18, 20, 21, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 7, 9 , 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 8, 9 , 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 7, 10, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 8, 10, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 7, 11, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 8, 11, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 7, 12, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(3, 8, 12, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 7, 9 , 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 8, 9 , 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 7, 10, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 8, 10, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 7, 11, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 8, 11, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 7, 12, 14, 16, 18, 20, 22, 24)));
        Assertions.assertTrue(productAutomaton.contains(state(4, 8, 12, 14, 16, 18, 20, 22, 24)));

        // check transitions
        Assertions.assertTrue(productAutomaton.containsTransition(state(1, 5, 9, 13, 15, 17, 19, 21, 23), state(2, 5, 9, 14, 16, 17, 19, 21, 23),   1));   // 1  2  1
        Assertions.assertTrue(productAutomaton.containsTransition(state(2, 5, 9, 14, 16, 17, 19, 21, 23), state(3, 5, 9, 14, 16, 17, 20, 21, 23),   2));   // 2  3  2
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 5, 9, 14, 16, 17, 20, 21, 23), state(3, 6, 9, 14, 16, 17, 20, 21, 24),   4));   // 3  4  4
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 6, 9, 14, 16, 17, 20, 21, 24), state(3, 7, 9, 14, 16, 18, 20, 22, 24),   5));   // 4  6  5
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 6, 9, 14, 16, 17, 20, 21, 24), state(4, 6, 9, 14, 16, 18, 20, 21, 24),   3));   // 4  5  3
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 6, 9, 14, 16, 18, 20, 21, 24), state(4, 7, 9, 14, 16, 18, 20, 22, 24),   5));   // 5  14 5
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 9, 14, 16, 18, 20, 22, 24), state(3, 8, 9, 14, 16, 18, 20, 22, 24),   6));   // 6  7  6
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 9, 14, 16, 18, 20, 22, 24), state(3, 7, 10, 14, 16, 18, 20, 22, 24),  7));  // 6  8  7
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 8, 9, 14, 16, 18, 20, 22, 24), state(3, 8, 10, 14, 16, 18, 20, 22, 24),  7));  // 7  9  7
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 10, 14, 16, 18, 20, 22, 24), state(3, 8, 10, 14, 16, 18, 20, 22, 24), 6)); // 8  9  6
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 10, 14, 16, 18, 20, 22, 24), state(3, 7, 11, 14, 16, 18, 20, 22, 24), 8)); // 8  10 8
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 8, 10, 14, 16, 18, 20, 22, 24), state(3, 8, 11, 14, 16, 18, 20, 22, 24), 8)); // 9  11 8
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 11, 14, 16, 18, 20, 22, 24), state(3, 8, 11, 14, 16, 18, 20, 22, 24), 6)); // 10 11 6
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 11, 14, 16, 18, 20, 22, 24), state(3, 7, 12, 14, 16, 18, 20, 22, 24), 9)); // 10 12 9
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 8, 11, 14, 16, 18, 20, 22, 24), state(3, 8, 12, 14, 16, 18, 20, 22, 24), 9)); // 11 13 9
        Assertions.assertTrue(productAutomaton.containsTransition(state(3, 7, 12, 14, 16, 18, 20, 22, 24), state(3, 8, 12, 14, 16, 18, 20, 22, 24), 6)); // 12 13 6
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 9, 14, 16, 18, 20, 22, 24), state(4, 8, 9, 14, 16, 18, 20, 22, 24),   6));   // 14 15 6
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 9, 14, 16, 18, 20, 22, 24), state(4, 7, 10, 14, 16, 18, 20, 22, 24),  7));  // 14 16 7
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 8, 9, 14, 16, 18, 20, 22, 24), state(4, 8, 10, 14, 16, 18, 20, 22, 24),  7));   // 15 17 7
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 10, 14, 16, 18, 20, 22, 24), state(4, 8, 10, 14, 16, 18, 20, 22, 24), 6)); // 16 17 6
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 10, 14, 16, 18, 20, 22, 24), state(4, 7, 11, 14, 16, 18, 20, 22, 24), 8)); // 16 18 8
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 8, 10, 14, 16, 18, 20, 22, 24), state(4, 8, 11, 14, 16, 18, 20, 22, 24), 8)); // 17 19 8
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 11, 14, 16, 18, 20, 22, 24), state(4, 8, 11, 14, 16, 18, 20, 22, 24), 6)); // 18 19 6
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 11, 14, 16, 18, 20, 22, 24), state(4, 7, 12, 14, 16, 18, 20, 22, 24), 9)); // 18 20 9
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 8, 11, 14, 16, 18, 20, 22, 24), state(4, 8, 12, 14, 16, 18, 20, 22, 24), 9)); // 19 21 9
        Assertions.assertTrue(productAutomaton.containsTransition(state(4, 7, 12, 14, 16, 18, 20, 22, 24), state(4, 8, 12, 14, 16, 18, 20, 22, 24), 6)); // 20 21 6
    }

    public static Graph manualBuildExecutionGraph()
    {
        Graph productAutomaton = new Graph();
        productAutomaton.addNode(node(1,  1, 5, 9 , 13, 15, 17, 19, 21, 23));
        productAutomaton.addNode(node(2,  2, 5, 9 , 14, 16, 17, 19, 21, 23));
        productAutomaton.addNode(node(3,  3, 5, 9 , 14, 16, 17, 20, 21, 23));
        productAutomaton.addNode(node(4,  3, 6, 9 , 14, 16, 17, 20, 21, 24));
        productAutomaton.addNode(node(5,  4, 6, 9 , 14, 16, 18, 20, 21, 24));
        productAutomaton.addNode(node(6,  3, 7, 9 , 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(7,  3, 8, 9 , 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(8,  3, 7, 10, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(9,  3, 8, 10, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(10, 3, 7, 11, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(11, 3, 8, 11, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(12, 3, 7, 12, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(13, 3, 8, 12, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(14, 4, 7, 9 , 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(15, 4, 8, 9 , 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(16, 4, 7, 10, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(17, 4, 8, 10, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(18, 4, 7, 11, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(19, 4, 8, 11, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(20, 4, 7, 12, 14, 16, 18, 20, 22, 24));
        productAutomaton.addNode(node(21, 4, 8, 12, 14, 16, 18, 20, 22, 24));

        productAutomaton.addTransition(state(1, 5, 9, 13, 15, 17, 19, 21, 23), state(2, 5, 9, 14, 16, 17, 19, 21, 23), 1);   // 1  2  1
        productAutomaton.addTransition(state(2, 5, 9, 14, 16, 17, 19, 21, 23), state(3, 5, 9, 14, 16, 17, 20, 21, 23), 2);   // 2  3  2
        productAutomaton.addTransition(state(3, 5, 9, 14, 16, 17, 20, 21, 23), state(3, 6, 9, 14, 16, 17, 20, 21, 24), 4);   // 3  4  4
        productAutomaton.addTransition(state(3, 6, 9, 14, 16, 17, 20, 21, 24), state(3, 7, 9, 14, 16, 18, 20, 22, 24), 5);   // 4  6  5
        productAutomaton.addTransition(state(3, 6, 9, 14, 16, 17, 20, 21, 24), state(4, 6, 9, 14, 16, 18, 20, 21, 24), 3);   // 4  5  3
        productAutomaton.addTransition(state(4, 6, 9, 14, 16, 18, 20, 21, 24), state(4, 7, 9, 14, 16, 18, 20, 22, 24), 5);   // 5  14 5
        productAutomaton.addTransition(state(3, 7, 9, 14, 16, 18, 20, 22, 24), state(3, 8, 9, 14, 16, 18, 20, 22, 24), 6);   // 6  7  6
        productAutomaton.addTransition(state(3, 7, 9, 14, 16, 18, 20, 22, 24), state(3, 7, 10, 14, 16, 18, 20, 22, 24), 7);  // 6  8  7
        productAutomaton.addTransition(state(3, 8, 9, 14, 16, 18, 20, 22, 24), state(3, 8, 10, 14, 16, 18, 20, 22, 24), 7);  // 7  9  7
        productAutomaton.addTransition(state(3, 7, 10, 14, 16, 18, 20, 22, 24), state(3, 8, 10, 14, 16, 18, 20, 22, 24), 6); // 8  9  6
        productAutomaton.addTransition(state(3, 7, 10, 14, 16, 18, 20, 22, 24), state(3, 7, 11, 14, 16, 18, 20, 22, 24), 8); // 8  10 8
        productAutomaton.addTransition(state(3, 8, 10, 14, 16, 18, 20, 22, 24), state(3, 8, 11, 14, 16, 18, 20, 22, 24), 8); // 9  11 8
        productAutomaton.addTransition(state(3, 7, 11, 14, 16, 18, 20, 22, 24), state(3, 8, 11, 14, 16, 18, 20, 22, 24), 6); // 10 11 6
        productAutomaton.addTransition(state(3, 7, 11, 14, 16, 18, 20, 22, 24), state(3, 7, 12, 14, 16, 18, 20, 22, 24), 9); // 10 12 9
        productAutomaton.addTransition(state(3, 8, 11, 14, 16, 18, 20, 22, 24), state(3, 8, 12, 14, 16, 18, 20, 22, 24), 9); // 11 13 9
        productAutomaton.addTransition(state(3, 7, 12, 14, 16, 18, 20, 22, 24), state(3, 8, 12, 14, 16, 18, 20, 22, 24), 6); // 12 13 6
        productAutomaton.addTransition(state(4, 7, 9, 14, 16, 18, 20, 22, 24), state(4, 8, 9, 14, 16, 18, 20, 22, 24), 6);   // 14 15 6
        productAutomaton.addTransition(state(4, 7, 9, 14, 16, 18, 20, 22, 24), state(4, 7, 10, 14, 16, 18, 20, 22, 24), 7);  // 14 16 7
        productAutomaton.addTransition(state(4, 8, 9, 14, 16, 18, 20, 22, 24), state(4, 8, 10, 14, 16, 18, 20, 22, 24),7);   // 15 17 7
        productAutomaton.addTransition(state(4, 7, 10, 14, 16, 18, 20, 22, 24), state(4, 8, 10, 14, 16, 18, 20, 22, 24), 6); // 16 17 6
        productAutomaton.addTransition(state(4, 7, 10, 14, 16, 18, 20, 22, 24), state(4, 7, 11, 14, 16, 18, 20, 22, 24), 8); // 16 18 8
        productAutomaton.addTransition(state(4, 8, 10, 14, 16, 18, 20, 22, 24), state(4, 8, 11, 14, 16, 18, 20, 22, 24), 8); // 17 19 8
        productAutomaton.addTransition(state(4, 7, 11, 14, 16, 18, 20, 22, 24), state(4, 8, 11, 14, 16, 18, 20, 22, 24), 6); // 18 19 6
        productAutomaton.addTransition(state(4, 7, 11, 14, 16, 18, 20, 22, 24), state(4, 7, 12, 14, 16, 18, 20, 22, 24), 9); // 18 20 9
        productAutomaton.addTransition(state(4, 8, 11, 14, 16, 18, 20, 22, 24), state(4, 8, 12, 14, 16, 18, 20, 22, 24), 9); // 19 21 9
        productAutomaton.addTransition(state(4, 7, 12, 14, 16, 18, 20, 22, 24), state(4, 8, 12, 14, 16, 18, 20, 22, 24), 6); // 20 21 6

        return productAutomaton;
    }

    private static List<Integer> state(Integer... state)
    {
        return Arrays.asList(state);
    }

    private static Node node(Integer nodeId, Integer... state)
    {
        return new Node(nodeId, Arrays.asList(state));
    }
}
