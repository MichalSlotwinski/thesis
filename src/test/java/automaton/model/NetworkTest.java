package automaton.model;

import automaton.util.Triple;
import automaton.util.Tuple;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NetworkTest
{
    @Test
    public void manualCustomNetworkTest()
    {
        Network network = manualCustomNetworkBuild();
        NetworkTest.checkNetwork(network);
    }

    public static void checkNetwork(Network network)
    {
        Automaton automaton = network.network.get(0);
        checkAutomaton(automaton, 1, 4, 3, 1);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(1, 2, 1)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(2, 3, 2)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(2), new Triple<>(3, 4, 3)), 0);

        automaton = network.network.get(1);
        checkAutomaton(automaton, 2, 4, 3, 5);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(5, 6, 4)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(6, 7, 5)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(2), new Triple<>(7, 8, 6)), 0);

        automaton = network.network.get(2);
        checkAutomaton(automaton, 3, 4, 3, 9);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(9, 10, 7)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(10, 11, 8)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(2), new Triple<>(11, 12, 9)), 0);

        automaton = network.network.get(3);
        checkAutomaton(automaton, 4, 2, 3, 13);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(13, 14, 1)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(13, 14, 4)), 1);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(14, 14, 4)), 0);

        automaton = network.network.get(4);
        checkAutomaton(automaton, 5, 2, 3, 15);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(15, 16, 1)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(16, 16, 2)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(16, 16, 5)), 1);

        automaton = network.network.get(5);
        checkAutomaton(automaton, 6, 2, 5, 17);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(17, 18, 3)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(17, 18, 5)), 1);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(18, 18, 5)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(18, 18, 6)), 1);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(18, 18, 9)), 2);

        automaton = network.network.get(6);
        checkAutomaton(automaton, 7, 2, 3, 19);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(19, 20, 2)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(20, 20, 4)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(20, 20, 5)), 1);

        automaton = network.network.get(7);
        checkAutomaton(automaton, 8, 2, 3, 21);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(21, 22, 5)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(22, 22, 7)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(22, 22, 8)), 1);

        automaton = network.network.get(8);
        checkAutomaton(automaton, 9, 2, 5, 23);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(23, 24, 4)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(0), new Triple<>(23, 24, 7)), 1);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(24, 24, 3)), 0);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(24, 24, 5)), 1);
        checkTransitions(new Tuple<Node, Triple<Integer, Integer, Integer>>(automaton.nodes.get(1), new Triple<>(24, 24, 7)), 2);
    }

    public static void checkAutomaton(Automaton automaton, Integer automatonId, Integer nNodes, Integer nTransitions, Integer currentNodeId)
    {
        Integer transitions = 0;
        Assertions.assertEquals(automatonId, automaton.id);
        Assertions.assertEquals(currentNodeId, automaton.current.id);
        Assertions.assertEquals(nNodes, automaton.nodes.size());
        for(Node node : automaton.nodes)
            transitions += node.neighbours.size();
        Assertions.assertEquals(nTransitions, transitions);
    }

    public static void checkTransitions(Tuple<Node, Triple<Integer, Integer, Integer>> check, Integer nodeId)
    {
        Assertions.assertEquals(check.second().first(), check.first().id);
        Assertions.assertEquals(check.second().second(), check.first().neighbours.get(nodeId).node.id);
        Assertions.assertEquals(check.second().third(), check.first().neighbours.get(nodeId).transitionId);
    }

    private static Network manualCustomNetworkBuild()
    {
        // Based on AutomatonNetwork.custom

        Network network = new Network();
        Automaton automaton1 = new Automaton(1);
        automaton1.addNode(new Node(1));
        automaton1.addNode(new Node(2));
        automaton1.addNode(new Node(3));
        automaton1.addNode(new Node(4));
        automaton1.addTransition(0, 1, 1);
        automaton1.addTransition(1, 2, 2);
        automaton1.addTransition(2, 3, 3);
        automaton1.setCurrent(1);
        network.addAutomaton(automaton1);

        Automaton automaton2 = new Automaton(2);
        automaton2.addNode(new Node(5));
        automaton2.addNode(new Node(6));
        automaton2.addNode(new Node(7));
        automaton2.addNode(new Node(8));
        automaton2.addTransition(0, 1, 4);
        automaton2.addTransition(1, 2, 5);
        automaton2.addTransition(2, 3, 6);
        automaton2.setCurrent(5);
        network.addAutomaton(automaton2);

        Automaton automaton3 = new Automaton(3);
        automaton3.addNode(new Node(9));
        automaton3.addNode(new Node(10));
        automaton3.addNode(new Node(11));
        automaton3.addNode(new Node(12));
        automaton3.addTransition(0, 1, 7);
        automaton3.addTransition(1, 2, 8);
        automaton3.addTransition(2, 3, 9);
        automaton3.setCurrent(9);
        network.addAutomaton(automaton3);

        Automaton automaton4 = new Automaton(4);
        automaton4.addNode(new Node(13));
        automaton4.addNode(new Node(14));
        automaton4.addTransition(0, 1, 1);
        automaton4.addTransition(0, 1, 4);
        automaton4.addTransition(1, 1, 4);
        automaton4.setCurrent(13);
        network.addAutomaton(automaton4);

        Automaton automaton5 = new Automaton(5);
        automaton5.addNode(new Node(15));
        automaton5.addNode(new Node(16));
        automaton5.addTransition(0, 1, 1);
        automaton5.addTransition(1, 1, 2);
        automaton5.addTransition(1, 1, 5);
        automaton5.setCurrent(15);
        network.addAutomaton(automaton5);

        Automaton automaton6 = new Automaton(6);
        automaton6.addNode(new Node(17));
        automaton6.addNode(new Node(18));
        automaton6.addTransition(0, 1, 3);
        automaton6.addTransition(0, 1, 5);
        automaton6.addTransition(1, 1, 5);
        automaton6.addTransition(1, 1, 6);
        automaton6.addTransition(1, 1, 9);
        automaton6.setCurrent(17);
        network.addAutomaton(automaton6);

        Automaton automaton7 = new Automaton(7);
        automaton7.addNode(new Node(19));
        automaton7.addNode(new Node(20));
        automaton7.addTransition(0, 1, 2);
        automaton7.addTransition(1, 1, 4);
        automaton7.addTransition(1, 1, 5);
        automaton7.setCurrent(19);
        network.addAutomaton(automaton7);

        Automaton automaton8 = new Automaton(8);
        automaton8.addNode(new Node(21));
        automaton8.addNode(new Node(22));
        automaton8.addTransition(0, 1, 5);
        automaton8.addTransition(1, 1, 7);
        automaton8.addTransition(1, 1, 8);
        automaton8.setCurrent(21);
        network.addAutomaton(automaton8);

        Automaton automaton9 = new Automaton(9);
        automaton9.addNode(new Node(23));
        automaton9.addNode(new Node(24));
        automaton9.addTransition(0, 1, 4);
        automaton9.addTransition(0, 1, 7);
        automaton9.addTransition(1, 1, 3);
        automaton9.addTransition(1, 1, 5);
        automaton9.addTransition(1, 1, 7);
        automaton9.setCurrent(23);
        network.addAutomaton(automaton9);

        return network;
    }
}
