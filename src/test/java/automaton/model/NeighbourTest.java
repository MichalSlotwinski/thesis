package automaton.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class NeighbourTest
{
    @Test
    public void neighbourTest()
    {
        Integer[] a1 = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        Integer[] a2 = new Integer[]{2, 2, 3, 4, 5, 6, 7, 8, 9};

        Integer[] a3 = new Integer[]{3, 2, 3, 4, 5, 6, 7, 8, 9};
        Integer[] a4 = new Integer[]{4, 2, 3, 4, 5, 6, 7, 8, 9};
        Node neighbour1 = new Node(3, Arrays.asList(a3));
        Node neighbour2 = new Node(4, Arrays.asList(a4));

        // ne == id ==
        Node n1 = new Node(1, Arrays.asList(a1));
        Node n2 = new Node(2, Arrays.asList(a2));
        n1.addNeighbour(neighbour1, 10);
        n2.addNeighbour(neighbour1, 10);
        Assertions.assertEquals(n1.neighbours.get(0),n2.neighbours.get(0));
        Assertions.assertTrue(n1.neighbours.get(0).similar(n2.neighbours.get(0)));

        // n == id !=
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(2, Arrays.asList(a2));
        n1.addNeighbour(neighbour1, 10);
        n2.addNeighbour(neighbour1, 1);
        Assertions.assertNotEquals(n1.neighbours.get(0),n2.neighbours.get(0));
        Assertions.assertTrue(n1.neighbours.get(0).similar(n2.neighbours.get(0)));
        
        // n != id ==
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(2, Arrays.asList(a2));
        n1.addNeighbour(neighbour1, 10);
        n2.addNeighbour(neighbour2, 10);
        Assertions.assertNotEquals(n1.neighbours.get(0),n2.neighbours.get(0));
        Assertions.assertFalse(n1.neighbours.get(0).similar(n2.neighbours.get(0)));
        
        // n != id !=
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(2, Arrays.asList(a2));
        n1.addNeighbour(neighbour1, 10);
        n2.addNeighbour(neighbour2, 1);
        Assertions.assertNotEquals(n1.neighbours.get(0),n2.neighbours.get(0));
        Assertions.assertFalse(n1.neighbours.get(0).similar(n2.neighbours.get(0)));
    }
}
