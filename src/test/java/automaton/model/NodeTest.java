package automaton.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;


public class NodeTest
{
    @Test
    public void nodeTest()
    {
        // id ==
        Node n1 = new Node(1);
        Node n2 = new Node(1);
        Assertions.assertEquals(n1, n2);
        Assertions.assertTrue(n1.similar(n2));

        // id !=
        n1 = new Node(1);
        n2 = new Node(2);
        Assertions.assertNotEquals(n1, n2);
        Assertions.assertTrue(n1.similar(n2));

        // id == l ==
        Integer[] a1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(1, Arrays.asList(a1));
        Assertions.assertEquals(n1, n2);
        Assertions.assertTrue(n1.similar(n2));

        // id == l !=
        Integer[] a2 = {2, 2, 3, 4, 5, 6, 7, 8, 9};
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(1, Arrays.asList(a2));
        Assertions.assertNotEquals(n1, n2);
        Assertions.assertFalse(n1.similar(n2));

        // id != l ==
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(2, Arrays.asList(a1));
        Assertions.assertNotEquals(n1, n2);
        Assertions.assertTrue(n1.similar(n2));

        // id != l !=
        n1 = new Node(1, Arrays.asList(a1));
        n2 = new Node(2, Arrays.asList(a2));
        Assertions.assertNotEquals(n1, n2);
        Assertions.assertFalse(n1.similar(n2));
    }
}
